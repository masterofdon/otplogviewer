﻿namespace OTPLogViewer
{
    partial class NewViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewViewForm));
            this.fileViewer1 = new System.Windows.Forms.DataGridView();
            this.fileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.startViewButton = new System.Windows.Forms.Button();
            this.selectDirectoriesButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.selectFilesButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fileViewer1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileViewer1
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.fileViewer1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Lucida Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fileViewer1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.fileViewer1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fileViewer1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileName,
            this.fileSize});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.fileViewer1.DefaultCellStyle = dataGridViewCellStyle6;
            this.fileViewer1.Location = new System.Drawing.Point(271, 12);
            this.fileViewer1.Name = "fileViewer1";
            this.fileViewer1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.fileViewer1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileViewer1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            this.fileViewer1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            this.fileViewer1.RowTemplate.ReadOnly = true;
            this.fileViewer1.Size = new System.Drawing.Size(603, 360);
            this.fileViewer1.TabIndex = 0;
            // 
            // fileName
            // 
            this.fileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fileName.HeaderText = "File Name";
            this.fileName.Name = "fileName";
            this.fileName.ReadOnly = true;
            this.fileName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fileName.Width = 480;
            // 
            // fileSize
            // 
            this.fileSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fileSize.HeaderText = "Size";
            this.fileSize.Name = "fileSize";
            this.fileSize.ReadOnly = true;
            this.fileSize.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fileSize.Width = 80;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // startViewButton
            // 
            this.startViewButton.Image = ((System.Drawing.Image)(resources.GetObject("startViewButton.Image")));
            this.startViewButton.Location = new System.Drawing.Point(918, 158);
            this.startViewButton.Name = "startViewButton";
            this.startViewButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.startViewButton.Size = new System.Drawing.Size(146, 56);
            this.startViewButton.TabIndex = 1;
            this.startViewButton.UseVisualStyleBackColor = true;
            this.startViewButton.Click += new System.EventHandler(this.OnStartViewButtonClick);
            // 
            // selectDirectoriesButton
            // 
            this.selectDirectoriesButton.FlatAppearance.BorderSize = 0;
            this.selectDirectoriesButton.Image = ((System.Drawing.Image)(resources.GetObject("selectDirectoriesButton.Image")));
            this.selectDirectoriesButton.Location = new System.Drawing.Point(0, 102);
            this.selectDirectoriesButton.Margin = new System.Windows.Forms.Padding(0);
            this.selectDirectoriesButton.Name = "selectDirectoriesButton";
            this.selectDirectoriesButton.Size = new System.Drawing.Size(200, 60);
            this.selectDirectoriesButton.TabIndex = 1;
            this.selectDirectoriesButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.selectFilesButton);
            this.panel1.Controls.Add(this.selectDirectoriesButton);
            this.panel1.Location = new System.Drawing.Point(24, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 360);
            this.panel1.TabIndex = 2;
            // 
            // selectFilesButton
            // 
            this.selectFilesButton.FlatAppearance.BorderSize = 0;
            this.selectFilesButton.Image = ((System.Drawing.Image)(resources.GetObject("selectFilesButton.Image")));
            this.selectFilesButton.Location = new System.Drawing.Point(0, 24);
            this.selectFilesButton.Margin = new System.Windows.Forms.Padding(0);
            this.selectFilesButton.Name = "selectFilesButton";
            this.selectFilesButton.Size = new System.Drawing.Size(200, 60);
            this.selectFilesButton.TabIndex = 2;
            this.selectFilesButton.UseVisualStyleBackColor = true;
            // 
            // NewViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 399);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.startViewButton);
            this.Controls.Add(this.fileViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewViewForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Create New View";
            ((System.ComponentModel.ISupportInitialize)(this.fileViewer1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView fileViewer1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button startViewButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSize;
        private System.Windows.Forms.Button selectDirectoriesButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button selectFilesButton;

    }
}