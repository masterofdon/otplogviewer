﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using FormPainter;

namespace FormPainter
{
    class ChartFormPainterMgr : FormPainterMgr
    {
        #region ATTRIBUTES
        private Panel _mChartGeneralContainer;
        #endregion
        #region EVENTS/EVENTHANDLERS
        private void OnMaxDayLeft(object sender, EventArgs e)
        {
            EnableButton("NextDay");
            EnableButton("NextRange");
        }
        private void OnMinDayLeft(object handler, EventArgs e)
        {
            EnableButton("PrevDay");
            EnableButton("PrevRange");
        }
        private void OnMaxDayReached(object sender, EventArgs e)
        {
            DisableButton("NextDay");
            DisableButton("NextRange");
        }
        private void OnMinDayReached(object sender, EventArgs e)
        {
            DisableButton("PrevDay");
            DisableButton("PrevRange");
        }
        private void OnMaxScaleReached(object sender, EventArgs e)
        {
            DisableButton("ZoomOut");
        }
        private void OnMinScaleReached(object sender, EventArgs e)
        {
            DisableButton("ZoomIn");
        }
        private void OnMaxScaleLeft(object sender, EventArgs e)
        {
            EnableButton("ZoomOut");
        }
        private void OnMinScaleLeft(object sender, EventArgs e)
        {
            EnableButton("ZoomIn");
        } 
        #endregion
        #region CONSTRUCTOR
        public ChartFormPainterMgr()
        {
            MessageBox.Show("ChartFormPainter must take 3 parameters:\nWidth,Height,Location");
            throw new InvalidOperationException();
        }
        public ChartFormPainterMgr(int pWidth,int pHeight, Point pLoc)
        {
            InitializeChartAreaContainerPanel(pWidth,pHeight,pLoc);
            InitializeChartToolBarPanelItems();
        }
        #endregion
        #region METHODS
        public override bool Insert()
        {
            return true;
        }
        public override object Query()
        {
            return null;
        }
        public override Panel PaintPanel(Point pOriginPoint, Size pSize, string pPanelTag)
        {
            return null;
        }
        private void InitializeChartAreaContainerPanel(int pWidth, int pHeight, Point pLocation)
        {
            Panel chartgeneralcontainer = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(pWidth, pHeight), Location = pLocation };
            Panel ChartAreaPanel = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(pWidth, 461) };
            Panel ChartToolsPanel = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(pWidth, 27) };
            chartgeneralcontainer.Tag = "ChartAreaContainerPanel";
            ChartAreaPanel.Tag = "ChartAreaPanel";
            ChartToolsPanel.Tag = "ChartToolBarStripPanel";
            chartgeneralcontainer.Controls.Add(ChartToolsPanel);
            chartgeneralcontainer.Controls.Add(ChartAreaPanel);
            DockAllControlsOfTypeTo(chartgeneralcontainer.Controls, new Panel(), DockStyle.Top);
            chartgeneralcontainer.BorderStyle = BorderStyle.None;
            _mChartGeneralContainer = chartgeneralcontainer;
        }
        public void InitializeChartToolBarPanelItems()
        {
            Panel _chartToolBarPanel = this.GetChartToolBarPanel();
            _chartToolBarPanel.BorderStyle = BorderStyle.None;
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton("-", "Zoom Out", "ZoomOut"));
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton("+", "Zoom In", "ZoomIn"));
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton(">>", "Next Range", "NextRange"));
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton(">", "Next Day", "NextDay"));
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton("<", "Previous Day", "PrevDay"));
            _chartToolBarPanel.Controls.Add(ChartFormPainter.CreateChartToolButton("<<", "Previous Range", "PrevRange"));
            DockAllControlsOfTypeTo(_chartToolBarPanel.Controls, new Button(), DockStyle.Left);
        }
        public Chart GetChart()
        {
            LinkedList<string> chartAreaLinkedList = new LinkedList<string>();
            chartAreaLinkedList.AddLast("ChartAreaContainerPanel");
            chartAreaLinkedList.AddLast("ChartAreaPanel");
            Panel ChartAreaPanel = GetChartAreaPanel();
            Chart temp = (Chart)ChartAreaPanel.Controls[0];
            return temp;
        }
        public Panel GetChartAreaPanel()
        {
            LinkedList<string> chartAreaLinkedList = new LinkedList<string>();
            chartAreaLinkedList.AddLast("ChartAreaContainerPanel");
            chartAreaLinkedList.AddLast("ChartAreaPanel");
            return RetrievePanelByKeyPanelPair(_mChartGeneralContainer.Controls, chartAreaLinkedList);
        }
        public Panel GetChartToolBarPanel()
        {
            LinkedList<string> _chartToolBarSelector = new LinkedList<string>();
            _chartToolBarSelector.AddLast("ChartAreaContainerPanel");
            _chartToolBarSelector.AddLast("ChartToolBarStripPanel");
            return RetrievePanelByKeyPanelPair(_mChartGeneralContainer.Controls, _chartToolBarSelector);
        }
        public Button GetChartToolBarButton(string pTag)
        {
            Panel charttoolbarpanel = GetChartToolBarPanel();
            foreach (Control iter in charttoolbarpanel.Controls)
            {
                Button iterx = (Button)iter;
                if ((string)iterx.Tag == pTag)
                    return iterx;
            }
            return null;
        }
        public void InitializeEmptyChart()
        {
            Chart chart1 = new Chart();
            ChartArea chart1Area = new ChartArea();
            chart1.ChartAreas.Add(chart1Area);
            chart1.Dock = DockStyle.Fill;
            Series erdem1 = new Series("Auth");
            erdem1.ChartType = SeriesChartType.Line;

            chart1.Series.Add(erdem1);
            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            chart1.Series[0].BorderWidth = 0;
            chart1.ChartAreas[0].AxisY.Enabled = AxisEnabled.True;
            chart1.Series[0].Points.AddY(0);
            chart1.Series[0].Points.AddY(20);
            chart1.Series[0].Points.AddY(40);
            chart1.Series[0].Points.AddY(60);
            chart1.Series[0].Points.AddY(80);
            chart1.ChartAreas[0].AxisX.Enabled = AxisEnabled.False;

            GetChartAreaPanel().Controls.Add(chart1);
        }
        public void InitializeEmptyChart2()
        {
            Chart chart1 = new Chart();
            ChartArea chart1Area = new ChartArea();
            chart1.ChartAreas.Add(chart1Area);
            chart1.Dock = DockStyle.Fill;
            Series erdem1 = new Series("Auth");
            erdem1.ChartType = SeriesChartType.Line;

            chart1.Series.Add(erdem1);
            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            chart1.Series[0].BorderWidth = 0;
            chart1.ChartAreas[0].AxisY.Enabled = AxisEnabled.True;
            chart1.Series[0].Points.AddY(0);
            chart1.Series[0].Points.AddY(20);
            chart1.Series[0].Points.AddY(40);
            chart1.Series[0].Points.AddY(60);
            chart1.Series[0].Points.AddY(80);
            chart1.ChartAreas[0].AxisX.Enabled = AxisEnabled.False;

            foreach (Control iter in _mainController.Controls)
            {
                if ((string)iter.Tag == "ChartAreaContainerPanel")
                {
                    foreach (Control iter2 in iter.Controls)
                    {
                        if ((string)iter2.Tag == "ChartAreaPanel")
                        {
                            iter2.Controls.Add(chart1);
                        }
                    }
                }
            }
            _mainController.Update();
        }
        public void EnableDisableAllButtons(bool enable)
        {
            if (enable)
            {
                EnableButton("NextRange");
                EnableButton("NextDay");
                EnableButton("PrevRange");
                EnableButton("PrevDay");
                EnableButton("ZoomIn");
                EnableButton("ZoomOut");
                return;
            }
            DisableButton("NextRange");
            DisableButton("NextDay");
            DisableButton("PrevRange");
            DisableButton("PrevDay");
            DisableButton("ZoomIn");
            DisableButton("ZoomOut");
        }
        private void ResizeChartAreaPanel()
        {

        }
        private void EnableButton(string pTag)
        {
            GetChartToolButton(pTag).Enabled = true;
        }
        private void DisableButton(string pTag)
        {
            GetChartToolButton(pTag).Enabled = false;
        }
        private Panel CreateChartAreaPanel()
        {
            Panel _chartAreaPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartAreaPanel.Tag = "ChartAreaPanel";
            _chartAreaPanel.Size = new Size((int)((float)_mainController.Size.Width * 0.7f), _mainController.Height - 99);
            return _chartAreaPanel;
        }
        private Panel CreateChartToolBarStripPanel()
        {
            Panel _chartToolBarPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartToolBarPanel.Tag = "ChartToolBarPanel";
            _chartToolBarPanel.Size = new Size((int)((float)_mainController.Size.Width * 0.7f), 25);
            return _chartToolBarPanel;
        }
        private Button CreateChartToolButton(string pDisplayText, string pToolTip, string pTag)
        {
            Button _chartToolItem = new Button { Size = ChartFormPainter.CHARTTOOLBUTTONSIZE };
            _chartToolItem.Text = pDisplayText;
            _chartToolItem.BackColor = Color.FromName("Control");
            _chartToolItem.Tag = pTag;
            _chartToolItem.Enabled = false;
            ToolTip x = new ToolTip();
            x.SetToolTip(_chartToolItem, pToolTip);
            return _chartToolItem;
        }
        private Button CreateChartToolButton(string pDisplayText, string pToolTip, string pTag, string pFileName)
        {
            Button _chartToolItem = new Button { Size = ChartFormPainter.CHARTTOOLBUTTONSIZE };
            _chartToolItem.BackgroundImage = (Image)(new Bitmap(Image.FromFile(pFileName), new Size(25, 25)));
            _chartToolItem.Text = pDisplayText;
            _chartToolItem.BackColor = Color.FromName("Control");
            _chartToolItem.Tag = pTag;
            _chartToolItem.Enabled = false;
            ToolTip x = new ToolTip();
            x.SetToolTip(_chartToolItem, pToolTip);
            return _chartToolItem;
        }
        private Button GetChartToolButton(string pTag)
        {
            Panel charttoolbarpanel = GetChartToolBarPanel();
            foreach (Control iter in charttoolbarpanel.Controls)
            {
                Button iterx = (Button)iter;
                if ((string)iterx.Tag == pTag)
                    return iterx;
            }
            return null;
        }
        private void PaintChartToolBarPanelItems()
        {
            MessageBox.Show(Application.StartupPath);
            Panel _chartToolBarPanel = this.GetChartToolBarPanel();
            _chartToolBarPanel.BorderStyle = BorderStyle.None;
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Zoom Out", "ZoomOut", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\zoomout.png"));
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Zoom In", "ZoomIn", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\zoomin.png"));
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Next Range", "NextRange", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\nextrange.png"));
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Next Day", "NextDay", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\nextday.png"));
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Previous Day", "PrevDay", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\prevday.png"));
            _chartToolBarPanel.Controls.Add(CreateChartToolButton("", "Previous Range", "PrevRange", @"D:\Users\aeekin\Desktop\OTPLogViewerV1.0.1\OTPLogViewer\Resources\prevrange.png"));
            DockAllControlsOfTypeTo(_chartToolBarPanel.Controls, new Button(), DockStyle.Left);
        }
        public void PaintChartAreaContainerPanel()
        {
            int _width = CalculateChartAreaContainerPanelSize().Width;
            int _mainControllerHeight = _mainController.Height;
            int _height = CalculateChartAreaContainerPanelSize().Height;
            Panel ChartGeneralContainerPanel = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(_width, _height), Location = new Point((int)((float)_mainController.Width * 0.12f) + 15, 25) };

            Panel ChartAreaPanel = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(_width, 461) };
            Panel ChartToolsPanel = new Panel { Margin = new Padding(0), Padding = new Padding(0), Size = new Size(_width, 27) };
            ChartGeneralContainerPanel.Tag = "ChartAreaContainerPanel";
            ChartAreaPanel.Tag = "ChartAreaPanel";
            ChartToolsPanel.Tag = "ChartToolBarStripPanel";
            ChartGeneralContainerPanel.Controls.Add(ChartToolsPanel);
            ChartGeneralContainerPanel.Controls.Add(ChartAreaPanel);

            DockAllControlsOfTypeTo(ChartGeneralContainerPanel.Controls, new Panel(), DockStyle.Top);
            ChartGeneralContainerPanel.BorderStyle = BorderStyle.None;
            this._mainController.Controls.Add(ChartGeneralContainerPanel);

            PaintChartToolBarPanelItems();
        }
        private Size CalculateChartAreaContainerPanelSize()
        {
            int _width = (int)((float)_mainController.Width * 0.865f);
            return new Size(_width, 490);
        }
        #endregion
    }
}
