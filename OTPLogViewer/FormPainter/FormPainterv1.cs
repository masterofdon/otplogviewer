﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace OTPLogViewer.FormPainter
{
    class FormPainterv1
    {
        public void CreateLeftListBoxPanel()
        {
            //_mainController.Controls.Add(PaintLeftListBoxContainerPanel(new Point(0,50),new Size(200,485),"ContainerPanel"));
            PaintLeftListBoxesContainerPanel(new Point(0, 50), new Size(200, 485), "ContainerPanel");
        }

        public Panel PaintLeftListBoxContainerPanel(Point pPanelOrigPoint, Size pPanelSize, string pPanelTag)
        {
            Panel _leftListBoxContainerPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxContainerPanel.Location = pPanelOrigPoint;
            _leftListBoxContainerPanel.Size = pPanelSize;
            _leftListBoxContainerPanel.BorderStyle = BorderStyle.None;

            _leftListBoxContainerPanel.Controls.Add(PaintRowPanel());
            _leftListBoxContainerPanel.Controls.Add(PaintRegistersPanel());
            _leftListBoxContainerPanel.Controls.Add(PaintGroupsPanel());
            _leftListBoxContainerPanel.Controls.Add(PaintNetworkElementsPanel());
            DockAllControlsOfTypeTo(_leftListBoxContainerPanel.Controls, _leftListBoxContainerPanel, DockStyle.Top);
            _leftListBoxContainerPanel.Tag = pPanelTag;
            return _leftListBoxContainerPanel;
        }

        public Panel PaintLeftListBoxPanel(Point pPanelOrigPoint, Size pPanelSize, string pPanelTag,
                                  Size pButtonSize, string pButtonText)
        {
            Panel _leftListBoxPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxPanel.Location = pPanelOrigPoint;
            _leftListBoxPanel.Size = pPanelSize;
            _leftListBoxPanel.BorderStyle = BorderStyle.None;
            ListBox _leftListBox = new ListBox { Padding = new Padding(0), Margin = new Padding(0) };
            Button _leftListButton = new Button { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListButton.Size = pButtonSize;
            _leftListButton.BackColor = Color.FromName("Control");
            _leftListButton.Text = pButtonText;
            _leftListBoxPanel.Controls.Add(_leftListBox);
            _leftListBoxPanel.Controls.Add(_leftListButton);
            switch (pPanelTag)
            {
                case "networkelementlist":
                    _leftListBox.SelectedIndexChanged += OnNetworkElementsListSelectedIndexChanged;
                    break;
                case "grouplist":
                    _leftListBox.SelectedIndexChanged += OnGroupListSelectedIndexChanged;
                    break;
                case "registerlist":
                    _leftListBox.SelectedIndexChanged += OnRegisterListSelectedIndexChanged;
                    break;
                case "rowlist":
                    _leftListBox.SelectedIndexChanged += OnRowSelectedIndexChanged;
                    break;
                default:
                    break;
            }
            _leftListBoxPanel.Tag = pPanelTag;
            return _leftListBoxPanel;
        }

        public Panel PaintNetworkElementsPanel()
        {
            Panel _networkElementListBoxPanel = PaintLeftListBoxPanel(new Point(0, 0), new Size(200, 121), "networkelementlist",
                                   new Size(200, 25), "Stream");
            DockAllControlsOfTypeTo(_networkElementListBoxPanel.Controls, new Button(), DockStyle.Top);
            DockAllControlsOfTypeTo(_networkElementListBoxPanel.Controls, new ListBox(), DockStyle.Top);
            _networkElementListBoxPanel.BackColor = Color.Red;
            return _networkElementListBoxPanel;
        }

        public Panel PaintGroupsPanel()
        {
            Panel _groupsListBoxPanel = PaintLeftListBoxPanel(new Point(0, 171), new Size(200, 121), "grouplist",
                                   new Size(200, 25), "Group");
            DockAllControlsOfTypeTo(_groupsListBoxPanel.Controls, new Button(), DockStyle.Top);
            DockAllControlsOfTypeTo(_groupsListBoxPanel.Controls, new ListBox(), DockStyle.Top);
            _groupsListBoxPanel.BackColor = Color.Gold;
            return _groupsListBoxPanel;
        }

        public Panel PaintRegistersPanel()
        {
            Panel _registerListBoxPanel = PaintLeftListBoxPanel(new Point(0, 292), new Size(200, 121), "registerlist",
                                   new Size(200, 25), "Register");
            DockAllControlsOfTypeTo(_registerListBoxPanel.Controls, new Button(), DockStyle.Top);
            DockAllControlsOfTypeTo(_registerListBoxPanel.Controls, new ListBox(), DockStyle.Top);
            _registerListBoxPanel.BackColor = Color.Red;
            return _registerListBoxPanel;
        }

        public Panel PaintRowPanel()
        {
            Panel _rowListBoxPanel = PaintLeftListBoxPanel(new Point(0, 413), new Size(200, 121), "rowlist",
                                   new Size(200, 25), "Row");
            DockAllControlsOfTypeTo(_rowListBoxPanel.Controls, new Button(), DockStyle.Top);
            DockAllControlsOfTypeTo(_rowListBoxPanel.Controls, new ListBox(), DockStyle.Top);
            _rowListBoxPanel.BackColor = Color.Gold;
            return _rowListBoxPanel;
        }
    }
}
