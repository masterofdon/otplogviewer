﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace FormPainter
{
    public static class SegmentFormPainter
    {
        public static Panel CreateNewLeftListBoxPanelItem(ListBoxPanelItemStruct LBPItemProperties, EventHandler SelectedIndexChangedHandler)
        {
            Panel _leftListBoxPanelItem = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxPanelItem.BorderStyle = BorderStyle.None;
            ListBox _leftListBoxPanelItemListBox = new ListBox { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxPanelItem.Size = LBPItemProperties.PanelSize;
            Button _leftListBoxPanelItemButton = new Button { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxPanelItemButton.Size = LBPItemProperties.ButtonSize;
            _leftListBoxPanelItemButton.BackColor = Color.FromName("Control");
            _leftListBoxPanelItemButton.Text = LBPItemProperties.ButtonText;
            _leftListBoxPanelItem.Controls.Add(_leftListBoxPanelItemListBox);
            _leftListBoxPanelItem.Controls.Add(_leftListBoxPanelItemButton);
            _leftListBoxPanelItemButton.Dock = DockStyle.Top;
            _leftListBoxPanelItemListBox.Dock = DockStyle.Top;
            _leftListBoxPanelItemListBox.SelectedIndexChanged += SelectedIndexChangedHandler;
            _leftListBoxPanelItem.Tag = LBPItemProperties.PanelTag;
            return _leftListBoxPanelItem;
        }
    }
}
