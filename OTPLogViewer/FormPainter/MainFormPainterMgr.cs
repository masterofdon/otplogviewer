﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace FormPainter
{
    public class MainFormPainterMgr : FormPainterMgr
    {
        #region ATTRIBUTES
        private ChartFormPainterMgr _mChartFormPainter;
        private SegmentFormPainterMgr _mSegmentFormPainter;
        private DashboardFormPainterMgr _mDashFormPainter;
        private ProgressBarFormPainterMgr _mProgressFormPainter;
        #endregion
        #region EVENTS/EVENTHANDLERS

        #endregion
        public MainFormPainterMgr()
        {
            
        }
        public MainFormPainterMgr(Form pController)
        {
            _mainController = pController;
        }

        public override bool Insert()
        {
            return true;
        }
        public override object Query()
        {
            return null;
        }
        public override Panel PaintPanel(Point pOriginPoint, Size pSize, string pPanelTag)
        {
            Panel generalContainer = new Panel();
            generalContainer.Size = pSize;
            generalContainer.Tag = pPanelTag;

            _mSegmentFormPainter.PaintPanel(pOriginPoint, pSize, "SegmentContainer");
            _mChartFormPainter.PaintPanel();
            _mDashFormPainter.PaintPanel();
            _mProgressFormPainter.PaintPanel();
            return generalContainer;
        }
    }
}
