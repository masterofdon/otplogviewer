﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormPainter
{
    public interface IFormPainter
    {
        void DoPaintTask();
        void DoPaintTask(PaintTask p);
        void GetPaintedElement();

    }
}
