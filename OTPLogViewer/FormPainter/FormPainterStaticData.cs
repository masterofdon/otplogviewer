﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormPainter
{
    public class FormPainterStaticData
    {
        public static string NELISTBOX = "networkelementlist";
        public static string GRLISTBOX = "grouplist";
        public static string REGLISTBOX = "registerlist";
        public static string ROWLISTBOX = "rowlist";

        public static string NEBUTTONTEXT = "Stream";
        public static string GRBUTTONTEXT = "Group";
        public static string REGBUTTONTEXT = "Register";
        public static string ROWBUTTONTEXT = "Row";

        public static string LEFTLISTBOXCONTAINERPANEL = "LeftListBoxContainerPanel";

        public static Size LEFTLISTBOXITEMLISTBOXSIZE
        {
            get
            {
                return new Size(200, 121);
            }
        }
        public static Size LEFTLISTBOXITEMBUTTONSIZE
        {
            get
            {
                return new Size(200, 25);
            }
        }

        public static Point LEFTLISTBOXPANELLOC
        {
            get
            {
                return new Point(0, 25);
            }
        }
        
    }
}
