﻿//==========================================================================
//OTP LogViewer Library - A Flexible OTP Log Analyzer in C#
//Copyright © 2015  Erdem Ahmet Ekin
//==========================================================================
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//==========================================================================
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//==========================================================================
//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//==========================================================================
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using OTPLogViewer.Properties;
using OTPLogViewer;
using LogStream.LogClasses;
using Utility.ModuleBuses;

namespace FormPainter
{
    public abstract class FormPainterMgr: IForumable
    {
        #region ATTRIBUTES
        protected Form _mainController;
        protected Enum ViewState;
        #endregion
        #region CONSTRUCTORS
        public FormPainterMgr()
        {
            MessageBox.Show("FormPainterMgr cannot be called without parameters.");
        }
        public FormPainterMgr(Form mc)
        {
            _mainController = mc;
        }      
        #endregion
        #region ABSTRACT METHODS
        public abstract Panel PaintPanel(string pPanelTag);
        public abstract Panel PaintPanel(Size pSize, string pPanelTag);
        public abstract Panel PaintPanel(Point pOriginPoint,Size pSize,string pPanelTag);
        public abstract bool Insert();
        public abstract object Query();
        #endregion
        #region METHODS
        public Panel RetrievePanelByKeyPanelPair(Control.ControlCollection col,LinkedList<string> keyList)
        {
            foreach (Control x in col)
            {
                if ((string)x.Tag == keyList.First.Value)
                {
                    Panel temp = (Panel)x;
                    if (keyList.Count == 1)
                        return temp;
                    else
                    {
                        keyList.RemoveFirst();
                        return RetrievePanelByKeyPanelPair(temp.Controls, keyList);
                    }
                }
            }
            return null;
        }
        #endregion
        #region UTILIZATION-METHODS
        protected void DockAllControlsOfTypeTo(Control.ControlCollection cCollection, Control pClassName, DockStyle ds)
        {
            foreach (Control iter in cCollection)
            {
                if (pClassName.GetType() == iter.GetType())
                    iter.Dock = ds;
            }
        }
        
        #endregion
    }
    
}
