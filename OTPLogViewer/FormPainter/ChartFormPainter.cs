﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using FormPainter;

namespace FormPainter
{
    public static class ChartFormPainter
    {
        #region STATIC ATTRIBUTES
        public static Size CHARTTOOLBUTTONSIZE = new Size(27, 27);
        #endregion
        #region EVENTS/EVENTHANDLERS

        #endregion
        #region METHODS
        public static Panel CreateChartAreaPanel(int pPanelWidth, int pPanelHeight)
        {
            Panel _chartAreaPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartAreaPanel.Tag = "ChartAreaPanel";
            _chartAreaPanel.Size = new Size(pPanelWidth, pPanelWidth);
            return _chartAreaPanel;
        }
        public static Panel CreateChartAreaPanel(int pPanelWidth, int pPanelHeight, string pPanelTag)
        {
            Panel _chartAreaPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartAreaPanel.Tag = pPanelTag;
            _chartAreaPanel.Size = new Size(pPanelWidth, pPanelWidth);
            return _chartAreaPanel;
        }
        public static Panel CreateChartToolBarStripPanel(int pPanelWidth, int pPanelHeight)
        {
            Panel _chartToolBarPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartToolBarPanel.Tag = "ChartToolBarPanel";
            _chartToolBarPanel.Size = new Size(pPanelWidth, 25);
            return _chartToolBarPanel;
        }
        public static Panel CreateChartToolBarStripPanel(int pPanelWidth, int pPanelHeight, string pPanelTag)
        {
            Panel _chartToolBarPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0), Dock = DockStyle.Top };
            _chartToolBarPanel.Tag = pPanelTag;
            _chartToolBarPanel.Size = new Size(pPanelWidth, 25);
            return _chartToolBarPanel;
        }
        public static Button CreateChartToolButton(string pDisplayText, string pToolTip, string pTag)
        {
            Button _chartToolItem = new Button { Size = new Size(43, 27) };
            _chartToolItem.Text = pDisplayText;
            _chartToolItem.BackColor = Color.FromName("Control");
            _chartToolItem.Tag = pTag;
            _chartToolItem.Enabled = false;
            ToolTip x = new ToolTip();
            x.SetToolTip(_chartToolItem, pToolTip);
            return _chartToolItem;
        }
        #endregion
    }
}
