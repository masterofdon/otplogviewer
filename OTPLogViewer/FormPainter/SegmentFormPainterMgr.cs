﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using LogStream.LogClasses;

namespace FormPainter
{
    class SegmentFormPainterMgr : FormPainterMgr
    {
        public delegate void NetworkElementsListBoxIndexChangedHandler(object sender, string e);
        public delegate void GroupSelectedHandler(object sender, string e);
        public delegate void RegisterSelectedHandler(object sender, string e);
        public delegate void RowSelectedHandler(object sender, string e);
        public event NetworkElementsListBoxIndexChangedHandler NetworkElementsListBoxIndexChanged;
        public event GroupSelectedHandler GroupListBoxSelected;
        public event RegisterSelectedHandler RegisterSelected;
        public event RowSelectedHandler RowSelected;

        public SegmentFormPainterMgr()
        {

        }
        public SegmentFormPainterMgr(Panel pLeftListBoxContainerPanel)
        {

        }
        //IForumable Methods
        public override bool Insert()
        {
            return true;
        }
        public override object Query()
        {
            return null;
        }
        public override Panel PaintPanel(string pPanelTag)
        {
            Panel _networkPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.NELISTBOX,
                    FormPainterStaticData.NEBUTTONTEXT), OnNetworkElementsListSelectedIndexChanged);
            Panel _groupPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.GRLISTBOX,
                    FormPainterStaticData.GRBUTTONTEXT), OnGroupListSelectedIndexChanged);
            Panel _registerPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.REGLISTBOX,
                    FormPainterStaticData.REGBUTTONTEXT), OnRegisterListSelectedIndexChanged);
            Panel _rowPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.ROWLISTBOX,
                    FormPainterStaticData.ROWBUTTONTEXT), OnRowSelectedIndexChanged);

            Panel _leftListBoxContainerPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxContainerPanel.Size = pPanelSize;
            _leftListBoxContainerPanel.BorderStyle = BorderStyle.None;
            _leftListBoxContainerPanel.Controls.Add(_rowPanel);
            _leftListBoxContainerPanel.Controls.Add(_registerPanel);
            _leftListBoxContainerPanel.Controls.Add(_groupPanel);
            _leftListBoxContainerPanel.Controls.Add(_networkPanel);
            DockAllControlsOfTypeTo(_leftListBoxContainerPanel.Controls, _leftListBoxContainerPanel, DockStyle.Top);
            _leftListBoxContainerPanel.Tag = pPanelTag;
            return _leftListBoxContainerPanel;
        }
        public override Panel PaintPanel(Size pPanelSize, string pPanelTag)
        {
            Panel _networkPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.NELISTBOX,
                    FormPainterStaticData.NEBUTTONTEXT), OnNetworkElementsListSelectedIndexChanged);
            Panel _groupPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.GRLISTBOX,
                    FormPainterStaticData.GRBUTTONTEXT), OnGroupListSelectedIndexChanged);
            Panel _registerPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.REGLISTBOX,
                    FormPainterStaticData.REGBUTTONTEXT), OnRegisterListSelectedIndexChanged);
            Panel _rowPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.ROWLISTBOX,
                    FormPainterStaticData.ROWBUTTONTEXT), OnRowSelectedIndexChanged);

            Panel _leftListBoxContainerPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxContainerPanel.Size = pPanelSize;
            _leftListBoxContainerPanel.BorderStyle = BorderStyle.None;
            _leftListBoxContainerPanel.Controls.Add(_rowPanel);
            _leftListBoxContainerPanel.Controls.Add(_registerPanel);
            _leftListBoxContainerPanel.Controls.Add(_groupPanel);
            _leftListBoxContainerPanel.Controls.Add(_networkPanel);
            DockAllControlsOfTypeTo(_leftListBoxContainerPanel.Controls, _leftListBoxContainerPanel, DockStyle.Top);
            _leftListBoxContainerPanel.Tag = pPanelTag;
            return _leftListBoxContainerPanel;
        }
        public override Panel PaintPanel(Point pPanelOrigPoint, Size pPanelSize, string pPanelTag)
        {
            Panel _networkPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.NELISTBOX,
                    FormPainterStaticData.NEBUTTONTEXT), OnNetworkElementsListSelectedIndexChanged);
            Panel _groupPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.GRLISTBOX,
                    FormPainterStaticData.GRBUTTONTEXT), OnGroupListSelectedIndexChanged);
            Panel _registerPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.REGLISTBOX,
                    FormPainterStaticData.REGBUTTONTEXT), OnRegisterListSelectedIndexChanged);
            Panel _rowPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.ROWLISTBOX,
                    FormPainterStaticData.ROWBUTTONTEXT), OnRowSelectedIndexChanged);

            Panel _leftListBoxContainerPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxContainerPanel.Location = pPanelOrigPoint;
            _leftListBoxContainerPanel.Size = pPanelSize;
            _leftListBoxContainerPanel.BorderStyle = BorderStyle.None;
            _leftListBoxContainerPanel.Controls.Add(_rowPanel);
            _leftListBoxContainerPanel.Controls.Add(_registerPanel);
            _leftListBoxContainerPanel.Controls.Add(_groupPanel);
            _leftListBoxContainerPanel.Controls.Add(_networkPanel);
            DockAllControlsOfTypeTo(_leftListBoxContainerPanel.Controls, _leftListBoxContainerPanel, DockStyle.Top);
            _leftListBoxContainerPanel.Tag = pPanelTag;
            return _leftListBoxContainerPanel;
        }
        private Panel PaintLeftListBoxesContainerPanel(Point pPanelOrigPoint, Size pPanelSize, string pPanelTag)
        {
            Panel _networkPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.NELISTBOX,
                    FormPainterStaticData.NEBUTTONTEXT), OnNetworkElementsListSelectedIndexChanged);
            Panel _groupPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.GRLISTBOX,
                    FormPainterStaticData.GRBUTTONTEXT), OnGroupListSelectedIndexChanged);
            Panel _registerPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.REGLISTBOX,
                    FormPainterStaticData.REGBUTTONTEXT), OnRegisterListSelectedIndexChanged);
            Panel _rowPanel = SegmentFormPainter.CreateNewLeftListBoxPanelItem(
                new ListBoxPanelItemStruct(
                    FormPainterStaticData.ROWLISTBOX,
                    FormPainterStaticData.ROWBUTTONTEXT), OnRowSelectedIndexChanged);

            Panel _leftListBoxContainerPanel = new Panel { Padding = new Padding(0), Margin = new Padding(0) };
            _leftListBoxContainerPanel.Location = pPanelOrigPoint;
            _leftListBoxContainerPanel.Size = pPanelSize;
            _leftListBoxContainerPanel.BorderStyle = BorderStyle.None;
            _leftListBoxContainerPanel.Controls.Add(_rowPanel);
            _leftListBoxContainerPanel.Controls.Add(_registerPanel);
            _leftListBoxContainerPanel.Controls.Add(_groupPanel);
            _leftListBoxContainerPanel.Controls.Add(_networkPanel);
            DockAllControlsOfTypeTo(_leftListBoxContainerPanel.Controls, _leftListBoxContainerPanel, DockStyle.Top);
            _leftListBoxContainerPanel.Tag = pPanelTag;
            return _leftListBoxContainerPanel;
        }
        public void PaintLeftListBoxPanel()
        {
            //_width = (int)((float)_mainController.Width * 0.125f);
            //PaintLeftListBoxesContainerPanel(FormPainterStaticData.LEFTLISTBOXPANELLOC, new Size(_width, 500), FormPainterStaticData.LEFTLISTBOXCONTAINERPANEL);
        }
        private void OnNetworkElementsListSelectedIndexChanged(object sender, EventArgs e)
        {
            int i = ((ListBox)sender).SelectedIndex;
            if (i == -1)
                return;
            string x = ((string)((ListBox)sender).Items[i]);
            string currentLBPTag = (string)((ListBox)sender).Parent.Tag;
            ClearDataOnNextLevelListBoxes(currentLBPTag);
            string nextLevelLBPTag = GetNextListBoxLevelTag(currentLBPTag);
            //TODO: Use the FORUM to get the Registers from LineReader;
            //LoadDataToStreamSegment(nextLevelLBPTag, ((MainForm)_mainController).LineReader.PopulateGroups(x));
        }
        private void OnGroupListSelectedIndexChanged(object sender, EventArgs e)
        {
            int i = ((ListBox)sender).SelectedIndex;
            if (i == -1)
                return;
            string x = ((string)((ListBox)sender).Items[i]);
            string currentLBPTag = (string)((ListBox)sender).Parent.Tag;
            ClearDataOnNextLevelListBoxes(currentLBPTag);
            string neTag = GetPreviousListBoxLevelTag(currentLBPTag);
            string ne = GetSelectedListBoxIndex(neTag);
            string nextLevelLBPTag = GetNextListBoxLevelTag(currentLBPTag);
            //TODO: Use the FORUM to get the Registers from LineReader;
            //LoadDataToStreamSegment(nextLevelLBPTag, ((MainForm)_mainController).LineReader.PopulateRegisters(ne, x));
        }
        private void OnRegisterListSelectedIndexChanged(object sender, EventArgs e)
        {
            int i = ((ListBox)sender).SelectedIndex;
            if (i == -1)
                return;
            string x = ((string)((ListBox)sender).Items[i]);
            string currentLBPTag = (string)((ListBox)sender).Parent.Tag;
            ClearDataOnNextLevelListBoxes(currentLBPTag);
            string grTag = GetPreviousListBoxLevelTag(currentLBPTag);
            string neTag = GetPreviousListBoxLevelTag(grTag);
            string group = GetSelectedListBoxIndex(grTag);
            string ne = GetSelectedListBoxIndex(neTag);
            string nextLevelLBPTag = GetNextListBoxLevelTag(currentLBPTag);
            //LoadDataToStreamSegment(nextLevelLBPTag, ((MainForm)_mainController).LineReader.PopulateRows(ne, group));
        }
        private void OnRowSelectedIndexChanged(object sender, EventArgs e)
        {
            int i = ((ListBox)sender).SelectedIndex;
            if (i == -1)
                return;
            string x = ((string)((ListBox)sender).Items[i]);
            RowSelectedHandler handler = RowSelected;
            if (handler != null)
            {
                handler(sender, x);
            }
        }
        private Panel GetPanelByTagInListBoxes(string key)
        {
            foreach (Control x in _mainController.Controls)
            {
                if ((string)x.Tag == "LeftListBoxContainerPanel")
                {
                    foreach (Control a in x.Controls)
                    {
                        if ((string)a.Tag == key)
                        {
                            return (Panel)a;
                        }
                    }
                }
            }
            return null;
        }
        private string GetSelectedListBoxIndex(string pListTag)
        {
            ListBox tempListBox = ((ListBox)this.GetPanelByTagInListBoxes(pListTag).Controls[0]);
            int _listboxIndex = tempListBox.SelectedIndex;
            string _listBoxIndexText = (string)tempListBox.Items[_listboxIndex];
            return _listBoxIndexText;
        }
        private string GetPreviousListBoxLevelTag(string pPreLBPTag)
        {
            string[] tempArray = { "networkelementlist", "grouplist", "registerlist", "rowlist" };
            for (int i = 0; i < tempArray.Length; i++)
            {
                if (tempArray[i] == pPreLBPTag && i != 0)
                {
                    return tempArray[i - 1];
                }
            }
            return null;
        }
        private string GetNextListBoxLevelTag(string pPreLBPTag)
        {
            string[] tempArray = { "networkelementlist", "grouplist", "registerlist", "rowlist" };
            for (int i = 0; i < tempArray.Length; i++)
            {
                if (tempArray[i] == pPreLBPTag && i != tempArray.Length - 1)
                {
                    return tempArray[i + 1];
                }
            }
            return null;
        }
        private void ClearDataOnStreamSegment(string segment)
        {
            Panel segmentPanel = this.GetPanelByTagInListBoxes(segment);
            if (segmentPanel != null)
            {
                foreach (Control iter in segmentPanel.Controls)
                {
                    if (iter.GetType() == new ListBox().GetType())
                    {
                        ((ListBox)iter).Items.Clear();
                        ((ListBox)iter).Update();
                        break;
                    }
                }
            }
        }
        public bool ClearDataOnNextLevelListBoxes(string pListBoxPanelTag)
        {
            string[] indexArray = { "networkelementlist", "grouplist", "registerlist", "rowlist" };
            List<string> tempList = indexArray.ToList();
            int index = -1;
            for (int i = 0; i < indexArray.Length; i++)
            {
                if (indexArray[i] == pListBoxPanelTag)
                {
                    index = i;
                }
            }
            if (index == -1)
                return false;
            tempList.RemoveRange(0, index + 1);
            ClearDataOnStreamSegment(tempList.ToArray());
            return true;
        }
        public void LoadDataToStreamSegment(string segment, string[] list)
        {
            Panel segmentPanel = this.GetPanelByTagInListBoxes(segment);
            if (segmentPanel != null)
            {
                foreach (Control iter in segmentPanel.Controls)
                {
                    if (iter.GetType() == new ListBox().GetType())
                    {
                        ListBox tempBox = (ListBox)iter;
                        foreach (string x in list)
                        {
                            tempBox.Items.Add(x);
                        }
                        break;
                    }
                }
            }
        }
        private void ClearDataOnStreamSegment(string[] segmentList)
        {
            foreach (string segment in segmentList)
            {
                ClearDataOnStreamSegment(segment);
            }
        }
        public OTPLogType GetSelectedOTPLogType()
        {
            string ne = this.GetSelectedListBoxIndex("networkelementlist");
            string group = this.GetSelectedListBoxIndex("grouplist");
            string register = this.GetSelectedListBoxIndex("registerlist");
            string row = this.GetSelectedListBoxIndex("rowlist");
            OTPLogType ret = new OTPLogType { NetworkElement = ne, Group = group, Register = register, Row = row };
            return ret;
        }
    }
    public class ListBoxPanelItemStruct
    {
        #region ATTRIBUTES
        Size _panelSize;
        string _panelTag;
        Size _buttonSize;
        string _buttonText;
        #endregion
        #region CONSTRUCTORS
        public ListBoxPanelItemStruct()
        {
        }
        public ListBoxPanelItemStruct(string pPanelTag, string pButtonText)
        {
            _panelSize = FormPainterStaticData.LEFTLISTBOXITEMLISTBOXSIZE;
            _panelTag = pPanelTag;
            _buttonSize = FormPainterStaticData.LEFTLISTBOXITEMBUTTONSIZE;
            _buttonText = pButtonText;
        }
        public ListBoxPanelItemStruct(Size pPanelSize, string pPanelTag, Size pButtonSize, string pButtonText)
        {
            _panelSize = pPanelSize;
            _panelTag = pPanelTag;
            _buttonSize = pButtonSize;
            _buttonText = pButtonText;
        }
        #endregion
        #region PROPERTIES
        public Size PanelSize
        {
            get { return _panelSize; }
            set { _panelSize = value; }
        }
        public string PanelTag
        {
            get { return _panelTag; }
            set { _panelTag = value; }
        }
        public Size ButtonSize
        {
            get { return _buttonSize; }
            set { _buttonSize = value; }
        }
        public string ButtonText
        {
            get { return _buttonText; }
            set { _buttonText = value; }
        }
        #endregion
    }
}
