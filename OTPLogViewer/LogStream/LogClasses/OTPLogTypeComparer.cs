﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogStream.LogClasses
{
    class OTPLogTypeComparer : IEqualityComparer<OTPLogType>
    {
        public OTPLogTypeComparer()
        {

        }

        public bool Equals(OTPLogType o1, OTPLogType o2)
        {
            if (o1.NetworkElement == o2.NetworkElement && o1.Group == o2.Group && o1.Register == o2.Register && o1.Row == o2.Row)
                return true;
            return false;
        }

        public int GetHashCode(OTPLogType x)
        {
            return 0;
        }

        public new string ToString()
        {
            return base.ToString();
        }

    }
}
