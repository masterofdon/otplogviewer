﻿//======
//OTP LogViewer Library - A Flexible OTP Log Analyzer in C#
//Copyright © 2015  Erdem Ahmet Ekin
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Date;
using Utility;

namespace LogStream.LogClasses
{
    class OTPLog
    {
        #region ATTRIBUTES
        private OTPLogType _mLogType;
        private int _mOfficeTransferPeriod;
        private DateTimePair _mStartDateTime;
        private DateTimePair _mEndDateTime;
        private List<ChartableData> _mLogData;
        #endregion
        #region CONSTRUCTORS
        public OTPLog()
        {
        }
        public OTPLog(OTPLogType pLogType, DateTimePair pStartDate, DateTimePair pEndDate)
        {
            _mLogType = pLogType;
            _mStartDateTime = pStartDate;
            _mEndDateTime = pEndDate;
        }
        public OTPLog(OTPLogType pLogType, int pOfficeTransferPeriod, DateTimePair pStartDate, DateTimePair pEndDate, List<ChartableData> pLogData)
        {
            _mLogType = pLogType;
            _mOfficeTransferPeriod = pOfficeTransferPeriod;
            _mStartDateTime = pStartDate;
            _mEndDateTime = pEndDate;
            _mLogData = pLogData;
        }
        #endregion
        #region PROPERTIES
        public OTPLogType LogType
        {
            get { return _mLogType; }
            set { _mLogType = value; }
        }
        public DateTimePair StartDateTime
        {
            get { return _mStartDateTime; }
            set { _mStartDateTime = value; }
        }
        public DateTimePair EndDateTime
        {
            get { return _mEndDateTime; }
            set { _mEndDateTime = value; }
        }
        public List<ChartableData> LogData
        {
            get { return _mLogData; }
            set { _mLogData = value; }
        }
        public int OfficeTransferPeriod
        {
            get { return _mOfficeTransferPeriod; }
            set { _mOfficeTransferPeriod = value; }
        }
        #endregion
    }
}
