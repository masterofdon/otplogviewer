﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogStream.LogClasses
{
    public class OTPLogType
    {
        #region ATTRIBUTES
        private string _ne;
        private string _gr;
        private string _re;
        private string _ro;
        #endregion     
        #region PROPERTIES
        public string NetworkElement
        {
            get { return _ne;}
            set { _ne = value; }
        }
        public string Group
        {
            get { return _gr; }
            set { _gr = value; }
        }
        public string Register
        {
            get { return _re; }
            set { _re = value; }
        }
        public string Row
        {
            get { return _ro; }
            set { _ro = value; }
        }
        #endregion
        #region CONSTRUCTORS

        public OTPLogType()
        {
            _ne = null;
            _gr = null;
            _re = null;
            _ro = null;
        }
        
        #endregion
        #region METHODS
        public override bool Equals(object obj)
        {
            if (_ne == ((OTPLogType)obj).NetworkElement && _gr == ((OTPLogType)obj).Group && _re == ((OTPLogType)obj).Register && _ro == ((OTPLogType)obj).Row)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public static bool operator == (OTPLogType o1, OTPLogType o2)
        {
            if(o1.NetworkElement == o2.NetworkElement && o1.Group == o2.Group && o1.Register == o2.Register && o1.Row == o2.Row)
                return true;
            return false;
        }

        public static bool operator !=(OTPLogType o1, OTPLogType o2)
        {
            if (o1.NetworkElement == o2.NetworkElement && o1.Group == o2.Group && o1.Register == o2.Register && o1.Row == o2.Row)
                return false;
            return true;
        }

        public override string ToString()
        {
            return this.NetworkElement + "," + this.Group + "," + this.Register + "," + this.Row;
        }
        #endregion
    }
}
