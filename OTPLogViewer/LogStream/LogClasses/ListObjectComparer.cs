﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogStream.LogClasses
{
    class ListObjectComparer : IEqualityComparer<object>
    {
        List<object> omdata;

        public ListObjectComparer(List<object> x)
        {
            omdata = x;
        }
        public new bool Equals(object obj,object obj1)
        {
            foreach(object a in omdata)
            {
                if (((string)a).Contains((string)obj1))
                {
                    return true;
                }
            }
            return false;
        }

        public int GetHashCode(object x)
        {
            return base.GetHashCode();
        }

        public string ToString(object x)
        {
            return base.ToString();
        }
    }
}
