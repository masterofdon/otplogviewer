﻿//======
//OTP LogViewer Library - A Flexible OTP Log Analyzer in C#
//Copyright © 2015  Erdem Ahmet Ekin
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LogStream.LogClasses;
using ChartManager;
using Utility.Date;

namespace LogStream.Readers
{
    public class LogReader
    {
        #region ATTRIBUTES
        private String[] fileList;
        private int _mOfficeTransferPeriod;
        private StreamReader sReader;
        private DateTime _lastDate;
        private DateTime _firstDate;
        #endregion
        #region CONSTRUCTORS
        public LogReader()
        {
        }       
        public LogReader(String[] list)
        {
            fileList = list;
        }
        #endregion
        #region PROPERTIES
        public DateTime LastDate
        {
            get { return _lastDate; }
            set { _lastDate = value; }
        }
        public DateTime FirstDate
        {
            get { return _firstDate; }
            set { _firstDate = value; }
        }
        #endregion
        #region METHODS
        public string[] PopulateNetworkElements()
        {
            SortedSet<string> networkElementSet = new SortedSet<string>();
            foreach(string f in fileList)
            {
                sReader = new StreamReader(f);
                string otpLine;
                string networkElement;
                while((otpLine = sReader.ReadLine()) != null)
                {
                    networkElement = (otpLine.Split(','))[2];
                    networkElementSet.Add(networkElement);
                }
                sReader.Close();
            }
            return networkElementSet.ToArray();
        }
        public string[] PopulateGroups(string networkElement)
        {
            SetLastAndFirstDateProperties(networkElement);
            SortedSet<string> groupSet = new SortedSet<string>();
            foreach(string f in fileList)
            {
                //If the network element is not the file.
                if (f.Contains(networkElement))
                {
                    sReader = new StreamReader(f);
                    string group;
                    string otpLine;
                    while ((otpLine = sReader.ReadLine()) != null)
                    {
                        group = (otpLine.Split(','))[3];
                        groupSet.Add(group);
                    }
                    sReader.Close();
                }
            }
            return groupSet.ToArray();
        }
        public string[] PopulateRegisters(string networkElement,string groupName)
        {
            SortedSet<string> registerSet = new SortedSet<string>();
            foreach (string f in fileList)
            {
                //If the network element is not the file.
                if (f.Contains(networkElement))
                {
                    sReader = new StreamReader(f);
                    string[] registerList;
                    string otpLine;
                    while ((otpLine = sReader.ReadLine()) != null)
                    {
                        if (otpLine.Contains(networkElement+","+groupName))
                        {
                            registerList = otpLine.Split(',');
                            for (int i = 5; i < registerList.Length; i+=2)
                            {
                                registerSet.Add(registerList[i]);
                            }
                            break;
                        }
                    }
                    sReader.Close();
                }
            }
            return registerSet.ToArray();
        }
        public string[] PopulateRows(string networkElement, string groupName)
        {
            SortedSet<string> rowSet = new SortedSet<string>();
            foreach (string f in fileList)
            {
                //If the network element is not the file.
                if (f.Contains(networkElement))
                {
                    sReader = new StreamReader(f);
                    string[] rowList;
                    string otpLine;
                    while ((otpLine = sReader.ReadLine()) != null)
                    {
                        if (otpLine.Contains(networkElement + "," + groupName))
                        {
                            rowList = otpLine.Split(',');
                            rowSet.Add(rowList[4]);
                        }
                    }
                    sReader.Close();
                }
            }
            return rowSet.ToArray();
        }
        [Obsolete("ReadOMDataForRow is deprecated, please use ReadOMData instead.", true)]
        public List<object> ReadOMDataForRow(string networkElement, string group, string register, string row)
        {
            List<object> tempArray = new List<object>();
            string searchString = networkElement + "," + group + "," + row;
            foreach (string f in fileList)
            {
                if (f.Contains(networkElement))
                {
                    sReader = new StreamReader(f);
                    string line;
                    while ((line = sReader.ReadLine()) != null)
                    {
                        if (line.Contains(searchString) && line.Contains(register))
                        {
                            string[] indexedEntries = line.Split(',');
                            int index = 0;
                            while (indexedEntries[index] != register)
                            {
                                index++;
                                if (index == indexedEntries.Length - 1)
                                {
                                    index = -1;
                                    break;
                                }
                            }
                            if (index != -1)
                                tempArray.Insert(0,indexedEntries[0] + "," + indexedEntries[1] + ";" + indexedEntries[index+1]);
                        }
                    }
                    sReader.Close();
                }
            }
            return tempArray;
        }
        public List<object> ReadOMData(OTPLogType pLogType,bool pSorted = true)
        {
            List<object> tempArray = new List<object>();
            string[] NEFileList = this.FilterFileListByNE(pLogType);
            string searchString = pLogType.NetworkElement + "," + pLogType.Group + "," + pLogType.Row;
            int lineIndex = 0;
            string firsttime ="", secondtime = "";
            foreach (string f in NEFileList)
            {
                string[] filenameArray = f.Split('\\');
                string fileName = filenameArray[filenameArray.Length - 1];
                if (fileName.Contains(pLogType.NetworkElement))
                {
                    sReader = new StreamReader(f);
                    string line;
                    while ((line = sReader.ReadLine()) != null)
                    {
                        if (line.Contains(searchString) && line.Contains(pLogType.Register))
                        {
                            string[] indexedEntries = line.Split(',');
                            int index = 0;
                            while (indexedEntries[index] != pLogType.Register)
                            {
                                index++;
                                if (index == indexedEntries.Length - 1)
                                {
                                    index = -1;
                                    break;
                                }
                            }
                            if (index != -1)
                                tempArray.Insert(0, indexedEntries[0] + "," + indexedEntries[1] + ";" + indexedEntries[index + 1]);
                            if (lineIndex == 0)
                            {
                                firsttime = indexedEntries[1];
                            }
                            else if (lineIndex == 1)
                            {
                                secondtime = indexedEntries[1];
                                if (firsttime.Equals("") || secondtime.Equals(""))
                                    throw new InvalidDataException();
                                FindOfficeTransferPeriod(firsttime, secondtime);
                            }
                            lineIndex++;
                        }
                    }
                    sReader.Close();
                }
            }            
            if(pSorted)
                tempArray.Sort();
            return tempArray;
        }
        public int GetOfficeTransferPeriod()
        {
            return _mOfficeTransferPeriod;
        }
        private void FindOfficeTransferPeriod(string p1, string p2)
        {
            string[] t1 = p1.Split(':');
            string[] t2 = p2.Split(':');
            int t1Hour, t1Min, t2Hour, t2Min;
            t1Hour = Int32.Parse(t1[0]);
            t1Min = Int32.Parse(t1[1]);
            t2Hour = Int32.Parse(t2[0]);
            t2Min = Int32.Parse(t2[1]);
            if(t1Hour > t2Hour)
                _mOfficeTransferPeriod = Math.Abs(Math.Abs(t1Hour - t2Hour) * 60 + t1Min - t2Min);
            else if (t1Hour == t2Hour)
                _mOfficeTransferPeriod = Math.Abs(t1Min - t2Min);
            else
            {
                _mOfficeTransferPeriod = Math.Abs(Math.Abs(t1Hour - t2Hour) * 60 + t2Min - t1Min);
            }
        }
        private void SetLastAndFirstDateProperties(string pNetworkElement)
        {
            SortedSet<DateTime> tempList = new SortedSet<DateTime>();
            string[] tempFileNameList = new string[fileList.Length];
            string[] tempFileList = FilterFileListByNE(pNetworkElement);
            foreach (string iter in tempFileList)
            {
                string[] fileNameArray = iter.Split('\\');
                string fileName = fileNameArray[fileNameArray.Length - 1];
                string tempDate = fileName.Split('_')[2];
                string[] date = tempDate.Split('-');
                tempList.Add(new DateTime(Int32.Parse(date[0]), Int32.Parse(date[1]), Int32.Parse(date[2])));
            }
            _firstDate = tempList.First();
            _lastDate = tempList.Last();
        }
        private string[] FilterFileListByNE(OTPLogType LogType)
        {
            List<string> tempFileList = new List<string>();
            //Method1: Apply Filter on Filenames.
            foreach(string f in fileList)
            {
                if (f.Contains(LogType.NetworkElement))
                {
                    tempFileList.Add(f);
                }
            }
            //TODO: Method2: Apply filter on file content specifically.
            //This method consumes more processing.
            return tempFileList.ToArray();
        }
        private string[] FilterFileListByNE(string pNetworkElement)
        {
            List<string> tempFileList = new List<string>();
            //Method1: Apply Filter on Filenames.
            foreach (string f in fileList)
            {
                if (f.Contains(pNetworkElement))
                {
                    tempFileList.Add(f);
                }
            }
            //TODO: Method2: Apply filter on file content specifically.
            //This method consumes more processing.
            return tempFileList.ToArray();
        }
        #endregion
    }
}
