﻿//======
//OTP LogViewer Library - A Flexible OTP Log Analyzer in C#
//Copyright © 2015  Erdem Ahmet Ekin
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LogStream.Readers
{
    internal class OTPLogReaderManager<T>
    {
        #region ATTRIBUTES
        private String[] fileList;
        private StreamReader sReader;
        private DateTime _lastDate;
        private DateTime _firstDate;
        #endregion
        #region CONSTRUCTORS
        public OTPLogReaderManager()
        {
        }
        public OTPLogReaderManager(String[] list)
        {
            fileList = list;
        }
        #endregion
        #region PROPERTIES
        public DateTime LastDate
        {
            get { return _lastDate; }
            set { _lastDate = value; }
        }
        public DateTime FirstDate
        {
            get { return _firstDate; }
            set { _firstDate = value; }
        }
        #endregion
        #region METHODS
        //public string[] PopulateNetworkElements();
        //public string[] PopulateGroups();
        //public string[] PopulateRegisters();
        //public string[] PopulateRows();
        //public List<T> ReadOMData();
        #endregion
    }
}
