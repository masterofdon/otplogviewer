﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;

namespace OTPLogViewer
{
    public partial class CreateViewOTPDirectoryButton : UserControl
    {
        Graphics gfx;
        Pen p = new Pen(Color.WhiteSmoke, 3.0f);

        public delegate void CreateViewOTPDirectoryClickHandler(object sender, EventArgs e);

        //Custom Events
        public event CreateViewOTPDirectoryClickHandler Clicked;


        public CreateViewOTPDirectoryButton()
        {
            InitializeComponent();       
        }

        private void registerAllEvents()
        {
        }


        protected override void OnLoad(EventArgs e)
        {           
            gfx = this.CreateGraphics();
        }

        protected void CreateViewOTPDirectoryButton_MouseEnter(object sender, EventArgs e)
        {
            this.Update();
            DrawBorder();
        }

        protected void CreateViewOTPDirectoryButton_MouseLeave(object sender, EventArgs e)
        {
            if (Cursor.Position.X < this.Location.X
                    || Cursor.Position.Y < this.Location.Y
                    || Cursor.Position.X > this.Location.X + this.Width-1
                    || Cursor.Position.Y > this.Location.Y + this.Height -1)
            {
                Invalidate();
            }
        }

        private void DrawBorder()
        {
            SolidBrush blueBrush = new SolidBrush(Color.WhiteSmoke);
            gfx.DrawRectangle(p, new Rectangle(new Point(0, 0), new Size(197, 57)));
        }

        private void CreateViewOTPDirectoryButton_MouseClick(object sender, MouseEventArgs e)
        {
            CreateViewOTPDirectoryClickHandler handler = Clicked;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

    }
}
