﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ModuleBuses
{
    public interface IForumable
    {
        object Query();
        bool Insert();
    }
}
