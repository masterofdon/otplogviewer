﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ModuleBuses
{
    class ManagerBusImpl
    {
        private CommonCommand _mCurrentCommand;
        private List<IForumable> _mAgentList;

        public delegate void CommonCommandReceivedHandler(object sender, CommonCommandReceivedEventArgs e);
        public event CommonCommandReceivedHandler CommonCommandReceived;

        public ManagerBusImpl()
        {
            _mAgentList = new List<IForumable>();
        }
        public void ProcessCommand()
        {

        }
        public void AddMemeber(IForumable p)
        {
            if (_mAgentList.Find(ByType(p)) == null)
                _mAgentList.Add(p);
        }
        public void RemoveMember(IForumable p)
        {
            if (_mAgentList.Find(ByType(p)) != null)
                _mAgentList.Remove(p);
        }
        public static Predicate<IForumable> ByType(IForumable p)
        {
            return delegate(IForumable forumable)
            {
                return forumable.GetType() == p.GetType();
            };
        }
    }
}
