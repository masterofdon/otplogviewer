﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Date
{
    public class DateTimePair
    {
        #region ATTRIBUTES
        private DateTime _date;
        private string _time;
        #endregion
        #region CONSTRUCTORS
        public DateTimePair()
        {
        }
        public DateTimePair(DateTime pDate)
        {
            _date = pDate;
            _time = "00:00:00";
        }
        public DateTimePair(DateTime pDate, string pTime)
        {
            string[] x = pTime.Split(':');
            _time = x[0]+":"+x[1]+":00";
            _date = new DateTime(pDate.Year, pDate.Month, pDate.Day, Int32.Parse(x[0]), Int32.Parse(x[1]), 0);
        }
        #endregion
        #region PROPERTIES
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        public string TimeString
        {
            get { return _time; }
            set
            {
                string[] x = value.Split(':');
                _time = x[0] + ":" + x[1] + ":00";
            }
        }
        public int Hour
        {
            get
            {
                string[] d = _time.Split(':');
                return Int32.Parse(d[0]);
            }
        }
        public int Minute
        {
            get
            {
                string[] d = _time.Split(':');
                return Int32.Parse(d[1]);
            }
        }
        #endregion
        #region METHODS
        public override bool Equals(object obj)
        {
            if ((DateTimePair)obj == this)
                return true;
            return false;
        }
        public override int GetHashCode()
        {
            return 0;
        }
        public override string ToString()
        {
            return _date.Month.ToString("D2") + "-" + _date.Day.ToString("D2") + "-" + _date.Year.ToString() + "," + _time;
        }
        public static int GetPeriod(DateTimePair lhs, DateTimePair rhs,int pPeriod)
        {
            lhs.Date = new DateTime(lhs.Date.Year, lhs.Date.Month, lhs.Date.Day, lhs.Hour, lhs.Minute, 0);
            rhs.Date = new DateTime(rhs.Date.Year, rhs.Date.Month, rhs.Date.Day, rhs.Hour, rhs.Minute, 0);
            TimeSpan x = lhs.Date - rhs.Date;
            int sum = Math.Abs(x.Days) * 24 * (((60 - pPeriod) / pPeriod) + 1) + Math.Abs(x.Hours) * 4 + ((Math.Abs(x.Minutes) + 15) / 15);
            return sum;
        }
        public void NextPeriod()
        {
            this._date = this._date.AddMinutes(15);
            string[] timer = _time.Split(':');
            switch (timer[1])
            {
                case "00":
                    timer[1] = "15";
                    break;
                case "15":
                    timer[1] = "30";
                    break;
                case "30":
                    timer[1] = "45";
                    break;
                case "45":
                    timer[1] = "00";
                    timer[0] = ((Int32.Parse(timer[0]) + 1)%24).ToString("D2");
                    break;
                default:
                    //error
                    break;
            }
            this._time = timer[0] + ":" + timer[1] + ":" + timer[2];
        }
        public void NextPeriod(int pMin)
        {
            this._date = this._date.AddMinutes(pMin);
            string[] timer = _time.Split(':');
            timer[0] = _date.Hour.ToString("D2");
            timer[1] = _date.Minute.ToString("D2");
            this._time = timer[0] + ":" + timer[1] + ":00";
        }
        public DateTimePair DeepCopy()
        {
            DateTimePair x = new DateTimePair();
            x.Date = this.Date;
            x.TimeString = this.TimeString;
            return x;
        }
        public static bool operator == (DateTimePair lhs, DateTimePair rhs)
        {
            if (lhs.Date.Day == rhs.Date.Day && lhs.Date.Month == rhs.Date.Month && lhs.Date.Year == rhs.Date.Year && lhs.TimeString.Equals(rhs.TimeString))
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(DateTimePair lhs, DateTimePair rhs)
        {
            if (lhs.Date.Day == rhs.Date.Day && lhs.Date.Month == rhs.Date.Month && lhs.Date.Year == rhs.Date.Year && lhs.TimeString.Equals(rhs.TimeString))
            {
                return false;
            }
            return true;
        }
        public static bool operator > (DateTimePair lhs,DateTimePair rhs)
        {
            if (lhs.Date.Year < rhs.Date.Year)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month < rhs.Date.Month)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day <= rhs.Date.Day)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day == rhs.Date.Day && lhs.Hour <= rhs.Hour)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day == rhs.Date.Day && lhs.Hour == rhs.Hour && lhs.Minute <= rhs.Minute)
                return false;
            return true;
        }
        public static bool operator < (DateTimePair lhs, DateTimePair rhs)
        {
            if (lhs.Date.Year > rhs.Date.Year)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month > rhs.Date.Month)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day > rhs.Date.Day)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day == rhs.Date.Day && lhs.Hour >= rhs.Hour)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day == rhs.Date.Day && lhs.Hour == rhs.Hour && lhs.Minute >= rhs.Minute)
                return false;
            return true;
        }
        public static bool operator >=(DateTimePair lhs, DateTimePair rhs)
        {
            if (lhs.Date.Year < rhs.Date.Year)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month < rhs.Date.Month)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day < rhs.Date.Day)
                return false;
            return true;
        }
        public static bool operator <=(DateTimePair lhs, DateTimePair rhs)
        {
            if (lhs.Date.Year > rhs.Date.Year)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month > rhs.Date.Month)
                return false;
            if (lhs.Date.Year == rhs.Date.Year && lhs.Date.Month == rhs.Date.Month && lhs.Date.Day > rhs.Date.Day)
                return false;
            return true;
        }
        #endregion
    }
}
