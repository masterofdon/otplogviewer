﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Date;

namespace Utility
{
    class ChartableData
    {
        private DateTimePair _mXValue;
        private string _mYValue;

        public ChartableData(DateTimePair pDate, string pValue)
        {
            _mXValue = pDate;
            _mYValue = pValue;
        }
        public DateTimePair XValue
        {
            get { return _mXValue; }
        }
        public string YValue
        {
            get { return _mYValue; }
        }
    }
}
