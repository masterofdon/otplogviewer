﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Date;

namespace Utility
{
    public class OTPLogChartData : AbstractChartableData
    {
        #region CONSTRUCTORS
        public OTPLogChartData()
        {}
        public OTPLogChartData(DateTimePair pStartDate,DateTimePair pEndDate) 
        {
            this._mStartDatePair = pStartDate;
            this._mEndDatePair = pEndDate;
        }
        #endregion

        #region PROPERTIES

        #endregion
    }
}
