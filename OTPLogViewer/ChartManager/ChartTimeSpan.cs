﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Date;

namespace ChartManager
{
    public class ChartTimeSpan
    {
        #region ATTRIBUTES
        DateTimePair _endDateTime;
        DateTimePair _startDateTime;
        int _daySpanned;
        #endregion
        #region CONSTRUCTORS
        public ChartTimeSpan(DateTimePair dT,int dS)
        {
            _endDateTime = new DateTimePair(dT.Date,"23:45:00");
            _startDateTime = new DateTimePair(dT.Date.AddDays(-1 * (dS-1)), "00:00:00");
            _daySpanned = dS;
        }
        #endregion
        #region PROPERTIES
        public DateTimePair EndDate
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }
        public int DaySpanned
        {
            get { return _daySpanned; }
            set { _daySpanned = value; }
        }
        #endregion
    }
}
