﻿//======
//OTP LogViewer Library - A Flexible OTP Log Analyzer in C#
//Copyright © 2015  Erdem Ahmet Ekin
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using LogStream.LogClasses;
using Utility.Date;
using Utility.ModuleBuses;
using OAM.TraceLog;
using OAM.TestForm;
using Utility;

namespace ChartManager
{
    class ChartManagerImpl
    {
        #region ATTRIBUTES
        private Chart _mChart;
        private ChartTimeSpan _mChartTimeSpan;
        private List<ChartableData> _mPoints;
        private OTPLog _mOTPLog;
        private Dictionary<int,int> _mDaysDict;
        #endregion
        #region EVENTS/EVENTHANDLERS
        public delegate void MaxDayReachedHandler(object sender,EventArgs e);
        public delegate void MinDayReachedHandler(object sender, EventArgs e);
        public delegate void MinDayLeftHandler(object handler, EventArgs e);
        public delegate void MaxDayLeftHandler(object sender, EventArgs e);
        public delegate void MaxScaleReachedHandler(object sender, EventArgs e);
        public delegate void MaxScaleLeftHandler(object sender, EventArgs e);
        public delegate void MinScaleReachedHandler(object sender, EventArgs e);
        public delegate void MinScaleLeftHandler(object sender, EventArgs e);
        public delegate void OTPLogLoadedHandler(object sender, EventArgs e);
        public event MaxDayReachedHandler MaxDayReached;
        public event MinDayReachedHandler MinDayReached;
        public event MinDayLeftHandler MinDayLeft;
        public event MaxDayLeftHandler MaxDayLeft;
        public event MaxScaleReachedHandler MaxScaleReached;
        public event MaxScaleLeftHandler MaxScaleLeft;
        public event MinScaleReachedHandler MinScaleReached;
        public event MinScaleLeftHandler MinScaleLeft;
        public event OTPLogLoadedHandler OTPLogLoaded;
        #endregion
        #region CONSTRUCTORS
        public ChartManagerImpl()
        {
            _mChart = new Chart();
        }
        public ChartManagerImpl(Chart p)
        {
            _mChart = p;
        }
        public ChartManagerImpl(Chart pChart, ChartTimeSpan pCTS)
        {
            _mChart = pChart;
            ChartTimeSpan = pCTS;
            OTPLogLoaded += OnOTPLogLoaded;
            _mDaysDict = new Dictionary<int, int>();
            _mDaysDict.Add(1, 2);
            _mDaysDict.Add(2, 4);
            _mDaysDict.Add(4, 8);
            _mDaysDict.Add(7, 12);
            _mDaysDict.Add(30, 32);
        }
        #endregion
        #region PROPERTIES
        public Chart Chart
        {
            get { return _mChart; }
            set { _mChart = value; }
        }
        public ChartTimeSpan ChartTimeSpan
        {
            get { return _mChartTimeSpan; }
            set { _mChartTimeSpan = value; }
        }
        public OTPLog ChartLogData
        {
            get { return _mOTPLog; }
            set { _mOTPLog = value; }
        }
        #endregion
        #region METHODS
        public void InitializeChartControls()
        {
            //TODO:Call for ChartLoc from the FORUM.
        }
        public void DisplayDataOnChart()
        {
            DateTimePair sD = new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays(-1 * (_mChartTimeSpan.DaySpanned -1)),"00:00:00");
            DateTimePair eD = new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays(1), "00:00:00");
            SetInterval();
            int startIndex = GetDataIndexFromDateTimePair(sD);
            int endIndex = (startIndex + (_mChartTimeSpan.DaySpanned * (24 * (60 / _mOTPLog.OfficeTransferPeriod))));
            if (startIndex == 0)
            {
                MinDayReachedHandler handler = MinDayReached;
                if (handler != null)
                {
                    handler(this, new EventArgs());
                }
            }
            else
            {
                MinDayLeftHandler handler = MinDayLeft;
                if (handler != null)
                {
                    handler(this, new EventArgs());
                }
            }
            if (endIndex >= _mPoints.Count - 2)
            {
                MaxDayReachedHandler handlerx = MaxDayReached;
                if (handlerx != null)
                {
                    handlerx(this, new EventArgs());
                }
            }            
            else
            {
                MaxDayLeftHandler handler = MaxDayLeft;
                if (handler != null)
                {
                    handler(this, new EventArgs());
                }
            }
            if (TestForm.getInstance() != null)
                TestForm.getInstance().SetChartManager(this);
            if (TempTraceWindow.enabled)
            {
                MessageBox.Show("StartIndex: " + startIndex.ToString());
                MessageBox.Show("EndIndex: " + endIndex.ToString());
                MessageBox.Show("Number of displayed: " + (endIndex - startIndex + 1).ToString());
                MessageBox.Show(TestForm.getInstance().GetTextBoxValue().ToString());
            }            
            if(_mChartTimeSpan.DaySpanned == 7)
                SetMinMaxIndex(startIndex + 1, startIndex + 2020);
            else
                SetMinMaxIndex(startIndex + 1, endIndex + 1);
            if (TestForm.getInstance() != null)
                TestForm.getInstance().SetTextBoxValue(endIndex + 1);
        }
        private int GetDataIndexFromDateTimePair(DateTimePair pDate)
        {
            int index = 0;

            if (pDate < _mPoints[0].XValue)
            {
                return 0;
            }
            foreach (ChartableData iter in _mPoints)

            if (pDate < _mPoints[0].XValue)
            {
                return 0;
            }
            foreach (ChartableData iter in _mPoints)
            {
                if (iter.XValue == pDate)
                    return index;
                index++;
            }
            return -1;
        }
        public void LoadDataToChart(OTPLog pOmData)
        {
            _mOTPLog = pOmData;
            OTPLogLoadedHandler handler = OTPLogLoaded;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        private void OnOTPLogLoaded(object sender, EventArgs e)
        {
            _mPoints = new List<ChartableData>();
            DateTimePair firstDateTimePair = new DateTimePair(_mOTPLog.LogData[0].XValue.Date, "00:00:00");
            DateTimePair firstDataDateTimePair = _mOTPLog.LogData[0].XValue;
            DateTimePair lastDateTimePair = new DateTimePair(_mOTPLog.LogData[_mOTPLog.LogData.Count - 1].XValue.Date.AddDays(1), "00:00:00");
            DateTimePair lastDataDateTimePair = _mOTPLog.LogData[_mOTPLog.LogData.Count - 1].XValue;
            DateTimePair indexDateTimePair = firstDateTimePair.DeepCopy();
            int totalValues = DateTimePair.GetPeriod(firstDateTimePair, lastDateTimePair, _mOTPLog.OfficeTransferPeriod);
            InitiateChartForData();
            int index = 0;
            for (int i = 0; i < totalValues; i++)
            {
                string xValue = indexDateTimePair.Date.Hour.ToString("D2") + ":" + indexDateTimePair.Date.Minute.ToString("D2");
                if (indexDateTimePair.Hour == 0 && indexDateTimePair.Minute == 0)
                    xValue = xValue + "\n\n" + DateFormatter(indexDateTimePair);

                if (index < _mOTPLog.LogData.Count && _mOTPLog.LogData[index].XValue == indexDateTimePair)
                {
                    _mChart.Series[0].Points.AddXY(xValue, _mOTPLog.LogData[index].YValue);
                    _mPoints.Add(new ChartableData(indexDateTimePair.DeepCopy(), _mOTPLog.LogData[index].YValue));
                    index++;
                }
                else
                {
                    _mChart.Series[0].Points.AddXY(xValue, -1);
                    _mPoints.Add(new ChartableData(indexDateTimePair.DeepCopy(), "-1"));
                    _mChart.Series[0].Points[_mChart.Series[0].Points.Count - 1].IsEmpty = true;
                }
                indexDateTimePair.NextPeriod(_mOTPLog.OfficeTransferPeriod);
            }
            totalValues -= 1;
        }
        public void GetPreviousTimeSpanRange()
        {
            DateTime tempDate = _mChartTimeSpan.EndDate.Date.AddDays(-1 * _mChartTimeSpan.DaySpanned);
            if(tempDate < _mOTPLog.LogData[0].XValue.Date)
            {
                MinDayReachedHandler handler = MinDayReached;
                if (handler != null)
                {
                    handler(tempDate, new EventArgs());
                }
            }
            ChartTimeSpan = new ChartTimeSpan(new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays((-1) * _mChartTimeSpan.DaySpanned),"00:00:00"), _mChartTimeSpan.DaySpanned);
        }
        public void GetNextTimeSpanRange()
        {
            DateTime tempDate = _mChartTimeSpan.EndDate.Date.AddDays(_mChartTimeSpan.DaySpanned);

            if (tempDate > _mPoints[_mPoints.Count - 1].XValue.Date)

            if (tempDate > _mOTPLog.LogData[_mOTPLog.LogData.Count - 1].XValue.Date)

            {
                MinDayReachedHandler handler = MinDayReached;
                if (handler != null)
                {
                    handler(tempDate, new EventArgs());
                }
            }
            ChartTimeSpan = new ChartTimeSpan(new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays(_mChartTimeSpan.DaySpanned),"00:00:00"), _mChartTimeSpan.DaySpanned);
        }
        public void GetNextDay()
        {
            DateTime tempDate = _mChartTimeSpan.EndDate.Date.AddDays(1);

            if (tempDate > _mPoints[_mPoints.Count - 1].XValue.Date)

            if (tempDate > _mOTPLog.LogData[_mOTPLog.LogData.Count - 1].XValue.Date)

            {
                MinDayReachedHandler handler = MinDayReached;
                if (handler != null)
                {
                    handler(tempDate, new EventArgs());
                }
            }
            ChartTimeSpan = new ChartTimeSpan(new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays(1), "00:00:00"), _mChartTimeSpan.DaySpanned);
        }
        public void GetPrevDay()
        {
            DateTime tempDate = _mChartTimeSpan.EndDate.Date.AddDays(-1);

            if (tempDate < _mPoints[0].XValue.Date)

            if (tempDate < _mOTPLog.LogData[0].XValue.Date)

            {
                MinDayReachedHandler handler = MinDayReached;
                if (handler != null)
                {
                    handler(tempDate, new EventArgs());
                }
            }
            ChartTimeSpan = new ChartTimeSpan(new DateTimePair(_mChartTimeSpan.EndDate.Date.AddDays(-1), "00:00:00"), _mChartTimeSpan.DaySpanned);
        }
        public void DecreaseTimeSpan()
        {
            int tempDS = _mChartTimeSpan.DaySpanned;
            switch (_mChartTimeSpan.DaySpanned)
            {
                case 1:
                    break;
                case 2:
                    tempDS = 1;
                    break;
                case 4:
                    tempDS = 2;
                    break;
                case 7:
                    tempDS = 4;
                    break;
                case 30:
                    tempDS = 7;
                    {
                        MaxScaleLeftHandler handler = MaxScaleLeft;
                        if (handler != null)
                        {
                            handler(_mChartTimeSpan, new EventArgs());
                        }
                        break;
                    }
                default:
                    break;
            }
            ChartTimeSpan = new ChartTimeSpan(_mChartTimeSpan.EndDate, tempDS);
        }
        public void IncreaseTimeSpan()
        {
            int tempDS = _mChartTimeSpan.DaySpanned;
            switch (_mChartTimeSpan.DaySpanned)
            {
                case 1:
                    tempDS = 2;
                    {
                        MinScaleLeftHandler handler = MinScaleLeft;
                        if(handler != null)
                        {
                            handler(_mChartTimeSpan,new EventArgs());
                        }
                    }
                    break;
                case 2:
                    tempDS = 4;
                    break;
                case 4:
                    tempDS = 7;
                    break;
                case 7:
                    tempDS = 30;                    
                    break;
                case 30:
                default:
                    break;
            }
            ChartTimeSpan = new ChartTimeSpan(_mChartTimeSpan.EndDate, tempDS);
        }
        private void InitiateChartForData()
        {
            _mChart.Series.Clear();
            _mChart.ChartAreas[0].AxisX.Enabled = AxisEnabled.True;
            _mChart.ChartAreas[0].AxisX.MinorTickMark.Enabled = true;
            _mChart.Series.Add(new Series("Data"));
            _mChart.Series[0].ChartType = SeriesChartType.Line;
            _mChart.Series[0].BorderWidth = 3;
            _mChart.Series[0].MarkerStyle = MarkerStyle.Circle;
            _mChart.Series[0].MarkerSize = 6;
        }
        private void SetInterval()
        {
            int pDays = _mChartTimeSpan.DaySpanned;
            DateTime d1 = _mOTPLog.EndDateTime.Date;
            TimeSpan t1 = d1.Subtract(_mOTPLog.StartDateTime.Date);
            int daySpanned = t1.Days + 1;

            if (pDays >= daySpanned)
            {
                _mChartTimeSpan.DaySpanned = daySpanned;
                MaxScaleReachedHandler handler = MaxScaleReached;
                if (handler != null)
                {
                    handler(this, new EventArgs());
                }

            }
            else
            {
                MaxScaleLeftHandler handler = MaxScaleLeft;
                if (handler != null)
                {
                    handler(this, new EventArgs());
                }
            }
            try
            {
                _mChart.ChartAreas[0].AxisX.Interval = _mDaysDict[pDays] * (60 / _mOTPLog.OfficeTransferPeriod);
            }
            catch (KeyNotFoundException ke)
            {
                if(TempTraceWindow.enabled)
                    MessageBox.Show(ke.StackTrace);
            }
            finally
            {
                if (pDays == 1)
                {
                    {
                        MinScaleReachedHandler handler = MinScaleReached;
                        if (handler != null)
                        {
                            handler(_mChartTimeSpan, new EventArgs());
                        }
                    }

                }
                else if (pDays == 30)
                {
                    MaxScaleReachedHandler handler = MaxScaleReached;
                    if (handler != null)
                    {
                        handler(_mChartTimeSpan, new EventArgs());
                    }
                }
            }
            
        }
        private void SetMinMaxIndex(int pMin, int pMax)
        {
            _mChart.ChartAreas[0].AxisX.Minimum = pMin;
            _mChart.ChartAreas[0].AxisX.Maximum = pMax;
        }
        public void SetMaxIndexTest(int pMax)
        {
            _mChart.ChartAreas[0].AxisX.Maximum = pMax;
        }
        private string DateFormatter(DateTimePair pDate)
        {
            string[] monthTextArray = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            return pDate.Date.Day.ToString() + " " + monthTextArray[pDate.Date.Month] + " " + pDate.Date.Year;
        }
        #endregion
    }
}
