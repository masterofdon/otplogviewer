﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogStream.LogClasses;
using Utility;
using Utility.Date;

namespace ChartManager
{
    class ChartDataCache
    {
        #region ATTRIBUTES
        private Dictionary<OTPLogType, List<object>> _mOTPLogIndexMap;
        #endregion
        #region CONSTRUCTORS
        public ChartDataCache()
        {
            _mOTPLogIndexMap = new Dictionary<OTPLogType, List<object>>(new OTPLogTypeComparer());
        }
        #endregion
        #region METHODS
        public bool LookUp(OTPLogType pLogType)
        {
            if (_mOTPLogIndexMap.ContainsKey(pLogType))
            {
                return true;
            }
            return false;
        }
        public List<object> GetKey(OTPLogType pKey)
        {
            return _mOTPLogIndexMap[pKey];

        }
        public void AddOMDataList(OTPLogType pLogType, List<object> pDataList)
        {
            _mOTPLogIndexMap.Add(pLogType, pDataList);
            List<ChartableData> j = GetChartableData(pLogType);
        }
        public List<ChartableData> GetChartableData(OTPLogType pLogType)
        {
            List<object> x = _mOTPLogIndexMap[pLogType];
            List<ChartableData> tempDataList = new List<ChartableData>();
            foreach (object iter in x)
            {
                string[] a1 = ((string)iter).Split(';');
                string[] a2 = a1[0].Split(',');
                string[] dateArray = a2[0].Split('-');
                tempDataList.Add(new ChartableData(new DateTimePair(new DateTime(Int32.Parse(dateArray[2]),Int32.Parse(dateArray[0]),Int32.Parse(dateArray[1])),a2[1]),a1[1]));
            }
            return tempDataList;

        }
        public OTPLog GetOTPLog(OTPLogType pLogType)
        {
            
            List<object> x = _mOTPLogIndexMap[pLogType];
            List<ChartableData> tempDataList = new List<ChartableData>();
            foreach (object iter in x)
            {
                string[] a1 = ((string)iter).Split(';');
                string[] a2 = a1[0].Split(',');
                string[] dateArray = a2[0].Split('-');
                tempDataList.Add(new ChartableData(new DateTimePair(new DateTime(Int32.Parse(dateArray[2]), Int32.Parse(dateArray[0]), Int32.Parse(dateArray[1])), a2[1]), a1[1]));
            }
            OTPLog tempOTP = new OTPLog(pLogType,15, tempDataList[0].XValue, tempDataList[tempDataList.Count - 1].XValue, tempDataList);
            return tempOTP;

        }
        public OTPLog GetOTPLog(OTPLogType pLogType,int pOfficePeriod)
        {

            List<object> x = _mOTPLogIndexMap[pLogType];
            List<ChartableData> tempDataList = new List<ChartableData>();
            foreach (object iter in x)
            {
                string[] a1 = ((string)iter).Split(';');
                string[] a2 = a1[0].Split(',');
                string[] dateArray = a2[0].Split('-');
                tempDataList.Add(new ChartableData(new DateTimePair(new DateTime(Int32.Parse(dateArray[2]), Int32.Parse(dateArray[0]), Int32.Parse(dateArray[1])), a2[1]), a1[1]));
            }
            OTPLog tempOTP = new OTPLog(pLogType, pOfficePeriod, tempDataList[0].XValue, tempDataList[tempDataList.Count - 1].XValue, tempDataList);
            return tempOTP;

        }
        public List<ChartableData> GetChartableDataForDateRange(List<ChartableData> pList, DateTimePair pStartDateTime,DateTimePair pEndDateTime)
        {
            List<ChartableData> tempDataList = new List<ChartableData>();
            int startIndex = 0;
            int endIndex = pList.Count - 1;
            for (int i = 1; i < pList.Count; i++)
            {
                if (pStartDateTime < pList[0].XValue)
                {
                   break;
                }
                if (pStartDateTime == pList[i].XValue)
                {
                    startIndex = i;
                    break;
                }
            }
            for (int i = pList.Count - 1; i > 0; i--)
            {
                if (pEndDateTime > pList[endIndex].XValue)
                {
                    break;
                }
                if (pEndDateTime == pList[i].XValue)
                {
                    endIndex = i;
                    break;
                }
            }
            for (int i = startIndex; i < endIndex; i++)
            {
                tempDataList.Add(pList[i]);
            }
            return tempDataList;
        }
        #endregion
    }
}
