﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewStateMachine.CreateViewSM;
using System.IO;

namespace OTPLogViewer
{
    public partial class NewViewForm : Form
    {
        private int state;
        String[] fileList;

        private delegate void CreateViewStateChangeHandler(object sender, CreateViewEventArgs e);
        public delegate void StartNewProjectHandler(object sender, StartNewProjectEventArgs e);

        //
        // Summary:
        //     Occurs when view state has changed.
        private event CreateViewStateChangeHandler StateChange;
        public event StartNewProjectHandler ProjectStarting;


        public NewViewForm()
        {
            InitializeComponent();
            registerEvents();
            state = 0;
        }      

        private void OnStateChanged(object sender,CreateViewEventArgs e)
        {
            switch (e.getNewState())
            {
                case 1:
                    StartNewProject();
                    break;
            }
        }

        private void registerEvents()
        {
            StateChange += new CreateViewStateChangeHandler(this.OnStateChanged);
            selectFilesButton.Click += OnSelectFilesButtonClicked;
        }

        private void OnSelectFilesButtonClicked(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        //Select Files Individually
        private void CreateFilesTable(OpenFileDialog x)
        {
            int count = x.FileNames.Length;
            fileList = x.FileNames;
            foreach (string f in fileList)
            {
                FileInfo fileInfo = new FileInfo(f);
                string[] tempArray = f.Split('\\');
                long s1 = fileInfo.Length;
                string fnew = tempArray[tempArray.Length-1];
                string[] a = new string[] { fnew, (s1/1024).ToString() + " KB" };
                fileViewer1.Rows.Add(a);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {   
            CreateFilesTable(((OpenFileDialog)sender));
        }

        //Clears out the SubStateMachines.
        private void nullState()
        {
            state = 0;
        }

        private void OnStartViewButtonClick(object sender, EventArgs e)
        {
            StartNewProject();
        }

        private void StartNewProject()
        {
            StartNewProjectHandler handle = ProjectStarting;
            if (handle != null)
            {
                handle(this, new StartNewProjectEventArgs(this.fileList));
            }
        }



    }
}
