﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewStateMachine.CreateViewSM;
using OTPLogViewer.Properties;
using LogStream.Readers;
using LogStream.LogClasses;
using System.Diagnostics;
using System.Media;
using System.Windows.Forms.DataVisualization.Charting;
using FormPainter;
using ChartManager;
using Utility.Date;
using Utility;
using Utility.ModuleBuses;
using OAM.TraceLog;
using OAM.TestForm;

namespace OTPLogViewer
{
    public partial class MainForm : Form
    {
        #region ATTRIBUTES
        private LogReader _mLineReader;
        private ChartManagerImpl _mChartMgrImpl;
        private FormPainterMgr _mFormPainter;
        private MainFormPainterMgr _mMainFormPainter;
        private ChartFormPainterMgr _mChartFormPainter;
        private SegmentFormPainterMgr _mSegmentFormPainter;
        private ChartDataCache _mChartDataCache;
        private ManagerBusImpl _mFormPainterBus;
        #endregion
        #region CONSTRUCTORS
        public MainForm()
        {
            InitializeComponent();
            _mSegmentFormPainter = new SegmentFormPainterMgr();
            _mChartFormPainter = new ChartFormPainterMgr();
            _mFormPainterBus.AddMemeber(_mSegmentFormPainter);
            _mFormPainterBus.AddMemeber(_mChartFormPainter);
        }
        #endregion
        #region EVENTS/EVENTHANDLERS
        #region EVENTS & DELEGATES
        public delegate void MaxDayReachedHandler(object sender, EventArgs e);
        public delegate void MinDayReachedHandler(object sender, EventArgs e);
        public delegate void MinDayLeftHandler(object handler, EventArgs e);
        public delegate void MaxDayLeftHandler(object sender, EventArgs e);
        public delegate void MaxScaleReachedHandler(object sender, EventArgs e);
        public delegate void MinScaleReachedHandler(object sender, EventArgs e);
        public delegate void MaxScaleLeftHandler(object sender, EventArgs e);
        public delegate void MinScaleLeftHandler(object sender, EventArgs e);
        public event MaxDayReachedHandler MaxDayReached;
        public event MinDayReachedHandler MinDayReached;
        public event MinDayLeftHandler MinDayLeft;
        public event MaxDayLeftHandler MaxDayLeft;
        public event MaxScaleReachedHandler MaxScaleReached;
        public event MaxScaleLeftHandler MaxScaleLeft;
        public event MinScaleReachedHandler MinScaleReached;
        public event MinScaleLeftHandler MinScaleLeft;
        #endregion
        #region BACKGROUNDWORKER EVENTHANDLERS
        private void OnStreamDataLoadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _mSegmentFormPainter.LoadDataToStreamSegment("networkelementlist", _mLineReader.PopulateNetworkElements());
            ((BackgroundWorker)sender).Disposed += OnStreamDataLoadBwDisposed;
            ((BackgroundWorker)sender).Dispose();
        }
        private void OnStreamDataLoadBwDisposed(object sender, EventArgs e)
        {
            MessageBox.Show("OTP Logs Loaded Successfully.");
        }
        #endregion
        #region MAIN EVENTHANDLERS
        #region PROJECT EVENTHANDLERS
        private void OnCreateNewViewClicked(object sender, EventArgs e)
        {
            NewViewForm x = new NewViewForm();
            x.ProjectStarting += OnNewProjectStarting;
            x.ShowDialog();
        }
        private void OnNewProjectStarting(object sender, StartNewProjectEventArgs e)
        {
            _mChartDataCache = new ChartDataCache();
            ((NewViewForm)sender).Close();
            BackgroundWorker bw1 = new BackgroundWorker();
            bw1.RunWorkerCompleted += OnStreamDataLoadCompleted;
            bw1.RunWorkerAsync();
            _mLineReader = new LogReader(e.getFileList());
            this.Controls.Add(_mMainFormPainter.PaintPanel(new Point(0, 0), this.Size, "GeneralPanel"));
        }
        private void OnNewProjectStartingAlt(object sender, StartNewProjectEventArgs e)
        {
            _mChartDataCache = new ChartDataCache();
            ((NewViewForm)sender).Close();
            BackgroundWorker bw1 = new BackgroundWorker();
            bw1.RunWorkerCompleted += OnStreamDataLoadCompleted;
            bw1.RunWorkerAsync();
            _mLineReader = new LogReader(e.getFileList());
            _mChartMgrImpl.InitializeChartControls();
        }
        #endregion
        #region FORM PAINTER EVENTHANDLERS
        private void OnRowListBoxIndexChanged(object sender, string e)
        {
            Chart chart = _mChartFormPainter.GetChart();
            OTPLogType logLine = _mSegmentFormPainter.GetSelectedOTPLogType();
            ChartTimeSpan timeSpan = new ChartTimeSpan(new DateTimePair(_mLineReader.LastDate, "23:55:00"), 1);
            if (!_mChartDataCache.LookUp(logLine))
            {
                List<object> omdata = _mLineReader.ReadOMData(logLine);
                _mChartDataCache.AddOMDataList(logLine, omdata);
                OTPLog j = _mChartDataCache.GetOTPLog(logLine,_mLineReader.GetOfficeTransferPeriod());
                _mChartMgrImpl = new ChartManagerImpl(chart, timeSpan);
                _mChartMgrImpl.MaxScaleReached += OnMaxScaleReached;
                _mChartMgrImpl.MaxScaleLeft += OnMaxScaleLeft;
                _mChartMgrImpl.MinScaleReached += OnMinScaleReached;
                _mChartMgrImpl.MinScaleLeft += OnMinScaleLeft;
                _mChartMgrImpl.MaxDayReached += OnMaxDayReached;
                _mChartMgrImpl.MaxDayLeft += OnMaxDayLeft;
                _mChartMgrImpl.MinDayReached += OnMinDayReached;
                _mChartMgrImpl.MinDayLeft += OnMinDayLeft;
                _mChartMgrImpl.LoadDataToChart(j);
                _mChartMgrImpl.DisplayDataOnChart();
            }
            else
            {
                OTPLog j = _mChartDataCache.GetOTPLog(logLine);
                _mChartMgrImpl = new ChartManagerImpl(chart, timeSpan);
                _mChartMgrImpl.LoadDataToChart(j);
                _mChartMgrImpl.DisplayDataOnChart();
            }
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnMinScaleLeft(object sender, EventArgs e)
        {
            MinScaleLeftHandler handler = MinScaleLeft;
            if (handler != null)
                handler(this, new EventArgs());
        }
        private void OnMaxScaleLeft(object sender, EventArgs e)
        {
            MaxScaleLeftHandler handler = MaxScaleLeft;
            if(handler != null)
                handler(this, new EventArgs());
        }
        private void OnMinScaleReached(object sender, EventArgs e)
        {
            MinScaleReachedHandler handler = MinScaleReached;
            if (handler != null)
                handler(this, new EventArgs());
        }
        private void OnMaxScaleReached(object sender, EventArgs e)
        {
            MaxScaleReachedHandler handler = MaxScaleReached;
            if (handler != null)
                handler(this, new EventArgs());
        }
        private void OnZoomOutClicked(object sender, EventArgs e)
        {
            _mChartMgrImpl.IncreaseTimeSpan();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnZoomInClicked(object sender, EventArgs e)
        {
            _mChartMgrImpl.DecreaseTimeSpan();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnNextRangeClick(object sender, EventArgs e)
        {
            _mChartMgrImpl.GetNextTimeSpanRange();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnNextDayClicked(object sender, EventArgs e)
        {
            _mChartMgrImpl.GetNextDay();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnPrevDayClicked(object sender, EventArgs e)
        {
            _mChartMgrImpl.GetPrevDay();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnPrevRangeClicked(object sender, EventArgs e)
        {
            _mChartMgrImpl.GetPreviousTimeSpanRange();
            _mChartMgrImpl.DisplayDataOnChart();
            RemoveEventHandlersForChartToolBarItems();
            SetEventhandlersForChartToolBarItems();
        }
        private void OnMaxDayReached(object sender, EventArgs e)
        {
            MaxDayReachedHandler handler = MaxDayReached;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        private void OnMaxDayLeft(object sender, EventArgs e)
        {
            MaxDayLeftHandler handler = MaxDayLeft;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        private void OnMinDayReached(object sender, EventArgs e)
        {
            MinDayReachedHandler handler = MinDayReached;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        private void OnMinDayLeft(object sender, EventArgs e)
        {
            MinDayLeftHandler handler = MinDayLeft;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        #endregion        
        #endregion
        #endregion
        #region PROPERTIES
        public LogReader LineReader
        {
            get { return _mLineReader; }
            set { _mLineReader = value; }
        }
        #endregion
        #region UTILITY METHODS
        private void SetEventhandlersForChartToolBarItems()
        {
            Panel chartToolBarPanel = _mChartFormPainter.GetChartToolBarPanel();
            foreach (Control iter in chartToolBarPanel.Controls)
            {
                Button iterx = (Button)iter;
                switch (iterx.Tag.ToString())
                {
                    case "NextRange":
                        iterx.Click += OnNextRangeClick;
                        break;
                    case "NextDay":
                        iterx.Click += OnNextDayClicked;
                        break;
                    case "PrevDay":
                        iterx.Click += OnPrevDayClicked;
                        break;
                    case "PrevRange":
                        iterx.Click += OnPrevRangeClicked;
                        break;
                    case "ZoomIn":
                        iterx.Click += OnZoomInClicked;
                        break;
                    case "ZoomOut":
                        iterx.Click += OnZoomOutClicked;
                        break;
                    default:
                        break;
                }
            }
        }
        private void RemoveEventHandlersForChartToolBarItems()
        {
            Panel chartToolBarPanel = _mChartFormPainter.GetChartToolBarPanel();
            foreach (Control iter in chartToolBarPanel.Controls)
            {
                Button iterx = (Button)iter;
                switch (iterx.Tag.ToString())
                {
                    case "NextRange":
                        iterx.Click -= OnNextRangeClick;
                        break;
                    case "NextDay":
                        iterx.Click -= OnNextDayClicked;
                        break;
                    case "PrevDay":
                        iterx.Click -= OnPrevDayClicked;
                        break;
                    case "PrevRange":
                        iterx.Click -= OnPrevRangeClicked;
                        break;
                    case "ZoomIn":
                        iterx.Click -= OnZoomInClicked;
                        break;
                    case "ZoomOut":
                        iterx.Click -= OnZoomOutClicked;
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        private void OnTraceWindowClicked(object sender, EventArgs e)
        {
            if (!traceWindowToolStripMenuItem.Checked)
            {
                traceWindowToolStripMenuItem.Checked = true;
                TempTraceWindow.enabled = true;
            }
            else
            {
                traceWindowToolStripMenuItem.Checked = false;
                TempTraceWindow.enabled = false;
            }
            
        }
        private void OnTestToolWindowClicked(object sender, EventArgs e)
        {
            if(TestForm.getInstance() == null)
                TestForm.alloc();
            if (!TestForm.Shown)
                TestForm.getInstance().Show();
            else
                TestForm.getInstance().Close();
        }
    }
}
