﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChartManager;

namespace OAM.TestForm
{
    public partial class TestForm : Form
    {
        private static TestForm instance;
        private static ChartManagerImpl _mChartManager;
        public static bool Shown = false;
        public TestForm()
        {
            InitializeComponent();
        }
        public static void alloc()
        {
            instance = new TestForm();
        }
        public static TestForm getInstance()
        {
            return instance;
        }
        public void SetChartManager(object x)
        {
            _mChartManager = (ChartManagerImpl)x;
        }
        public int GetTextBoxValue()
        {
            if (textBox1.Text == "")
                return 0;
            return Int32.Parse(textBox1.Text);
        }
        public void SetTextBoxValue(int pValue)
        {
            textBox1.Text = pValue.ToString();
            vScrollBar1.Value = Int32.Parse(textBox1.Text);
        }
        private void OnTextBoxChanged(object sender, EventArgs e)
        {
            _mChartManager.SetMaxIndexTest(Int32.Parse(textBox1.Text));
        }

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            textBox1.Text = vScrollBar1.Value.ToString();
        }

        private void TestForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            TestForm.Shown = false;
            _mChartManager = null;
            instance = null;
        }

    }
}
