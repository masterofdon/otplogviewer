﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewStateMachine.CreateViewSM
{
    public class StartNewProjectEventArgs : EventArgs
    {
        string[] fileList;

        public StartNewProjectEventArgs()
        {
        }

        public StartNewProjectEventArgs(string[] x)
        {
            fileList = x;
        }

        public string[] getFileList()
        {
            return fileList;
        }

        public void setFileList(string[] l)
        {
            fileList = l;
        }


    }
}
