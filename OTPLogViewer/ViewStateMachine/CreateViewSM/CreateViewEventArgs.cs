﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewStateMachine.CreateViewSM
{
    class CreateViewEventArgs : EventArgs
    {

        private int newState;
        
        public CreateViewEventArgs()
        {
            newState = 0;
        }
        
        public CreateViewEventArgs(int x)
        {
            newState = x;
        }

        public void setNewState(int x)
        {
            newState = x;
        }

        public int getNewState()
        {
            return newState;
        }

        public override string ToString()
        {
            return "the new state is: " + this.newState.ToString();
        }
        
    }
}
