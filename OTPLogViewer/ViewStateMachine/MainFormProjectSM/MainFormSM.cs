﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewStateMachine.MainFormProjectSM
{
    class MainFormSM
    {
        #region EVENTS/EVENTHANDLERS

        public delegate void NewProjectButtonClickedHandler(object sender,EventArgs e);
        public delegate void NewProjectStartClickedHandler(object sender, EventArgs e);
        public delegate void NewProjectStartCompletedHandler(object sender, EventArgs e);
        public delegate void CloseProjectClickedHandler(object sender, EventArgs e);
        public delegate void ProjectStartingHandler(object sender, EventArgs e);
        public delegate void ProjectStartedHandler(object sender,EventArgs e);
        public delegate void ProjectStartedNESelectedHandler(object sender, EventArgs e);
        public delegate void ProjectStartedGRSelectedHandler(object sender, EventArgs e);
        public delegate void ProjectStartedREGSelectedHandler(object sender, EventArgs e);
        public delegate void ProjectStartedROWSelectedHandler(object sender, EventArgs e);

        public event NewProjectButtonClickedHandler NewProjectButtonClicked;
        public event NewProjectStartClickedHandler NewProjectStartClicked;
        public event NewProjectStartCompletedHandler NewProjectStartCompleted;
        public event CloseProjectClickedHandler CloseProjectClicked;
        public event ProjectStartingHandler ProjectStarting;
        public event ProjectStartedHandler ProjectStarted;
        public event ProjectStartedNESelectedHandler NESelected;
        public event ProjectStartedGRSelectedHandler GRSelected;
        public event ProjectStartedREGSelectedHandler REGSelected;
        public event ProjectStartedROWSelectedHandler ROWSelected;

        #endregion
        #region ATTRIBUTES

        //Attribute for StateEnumType
        public enum State { MainFormProjectNullState, 
                            MainFormNewProjectStartViewState, 
                            MainFormNewProjectStartingState, 
                            MainFormProjectViewCompletedState };        
        //Attribute for currentState
        private State _cs;

        #endregion
        #region  CONSTRUCTORS

        public MainFormSM()
        {
            this.CurrentState = State.MainFormProjectNullState;
        }

        #endregion
        #region PROPERTIES
        public State CurrentState
        {
            get { return _cs; }
            set { _cs = value; }
        }
        #endregion
    }
}
